module.exports = [
 {
   "Name": "Klimašauskas Saulius",
   "Occupation": "instituto direktorius",
   "Phone": 2234350,
   "Room": "V303",
   "Email0": "klimasau@ibt.lt",
   "Email1": "saulius.klimasauskas@bti.vu.lt",
   "Skyrius": "Administracija",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Smirnovas Vytautas",
   "Occupation": "direktoriaus pavaduotojas",
   "Phone": 22343629420,
   "Room": "C303",
   "Email0": "vytautas.smirnovas@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Administracija",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Noreikienė Danutė",
   "Occupation": "administratorė",
   "Phone": 2234366,
   "Room": "C525",
   "Email0": "noreikiene@ibt.lt",
   "Email1": "danute.noreikiene@bti.vu.lt",
   "Skyrius": "Administracija",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Žiūkaitė Janina",
   "Occupation": "vyriausioji specialistė",
   "Phone": 22343659463,
   "Room": "C526",
   "Email0": "ziuk@ibt.lt",
   "Email1": "janina.ziukaite@bti.vu.lt",
   "Skyrius": "Administracija",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Gasevičienė Vilma",
   "Occupation": "sekretorė",
   "Phone": 22343659461,
   "Room": "C526",
   "Email0": "office@bti.vu.lt",
   "Email1": "vilma.gaseviciene@gmc.vu.lt",
   "Skyrius": "Administracija",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Leonora Bilinskienė",
   "Occupation": "vyresnioji specialistė",
   "Phone": 22343659462,
   "Room": "C525",
   "Email0": "leonora.bilinskiene@gmc.vu.lt",
   "Email1": "",
   "Skyrius": "Administracija",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Rudokienė Eglutė",
   "Occupation": "chemikė tyrėja",
   "Phone": 2234370,
   "Room": "V417",
   "Email0": "egru@ibt.lt",
   "Email1": "eglute.rudokiene@bti.vu.lt",
   "Skyrius": "Sekvenavimo centras",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Šikšnys Virginijus",
   "Occupation": "skyriaus vedėjas, vyriausiasis mokslo darbuotojas",
   "Phone": 2234354,
   "Room": "V329",
   "Email0": "siksnys@ibt.lt",
   "Email1": "virginijus.siksnys@bti.vu.lt",
   "Skyrius": "Baltymų-nukleorūgščių sąveikos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Gražulis Saulius",
   "Occupation": "vyriausiasis mokslo darbuotojas",
   "Phone": 2234353,
   "Room": "V323",
   "Email0": "grazulis@ibt.lt",
   "Email1": "saulius.grazulis@bti.vu.lt",
   "Skyrius": "Baltymų-nukleorūgščių sąveikos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Sasnauskas Giedrius",
   "Occupation": "vyriausiasis mokslo darbuotojas",
   "Phone": 2234443,
   "Room": "V331",
   "Email0": "gsasnaus@ibt.lt",
   "Email1": "giedrius.sasnauskas@bti.vu.lt",
   "Skyrius": "Baltymų-nukleorūgščių sąveikos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Zaremba Mindaugas",
   "Occupation": "vyriausiasis mokslo darbuotojas",
   "Phone": 2234357,
   "Room": "V335",
   "Email0": "zare@ibt.lt",
   "Email1": "mindaugas.zaremba@bti.vu.lt",
   "Skyrius": "Baltymų-nukleorūgščių sąveikos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Manakova Elena",
   "Occupation": "vyresnioji mokslo darbuotoja",
   "Phone": 2234353,
   "Room": "V323",
   "Email0": "lena@ibt.lt",
   "Email1": "elena.manakova@bti.vu.lt",
   "Skyrius": "Baltymų-nukleorūgščių sąveikos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Tamulaitienė Giedrė",
   "Occupation": "vyresnioji mokslo darbuotoja",
   "Phone": 2234357,
   "Room": "V335",
   "Email0": "eigie@ibt.lt",
   "Email1": "giedre.tamulaitiene@bti.vu.lt",
   "Skyrius": "Baltymų-nukleorūgščių sąveikos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Tamulaitis Gintautas",
   "Occupation": "vyriausiasis mokslo darbuotojas",
   "Phone": 9428,
   "Room": "V341",
   "Email0": "gitamul@ibt.lt",
   "Email1": "gintautas.tamulaitis@bti.vu.lt",
   "Skyrius": "Baltymų-nukleorūgščių sąveikos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Gasiūnas Giedrius",
   "Occupation": "vyresnysis mokslo darbuotojas",
   "Phone": 2234443,
   "Room": "V331",
   "Email0": "gasiunas@ibt.lt",
   "Email1": "giedrius.gasiunas@bti.vu.lt",
   "Skyrius": "Baltymų-nukleorūgščių sąveikos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Šilanskas Arūnas",
   "Occupation": "mokslo darbuotojas",
   "Phone": 9426,
   "Room": "V337",
   "Email0": "shilas@ibt.lt",
   "Email1": "arunas.silanskas@bti.vu.lt",
   "Skyrius": "Baltymų-nukleorūgščių sąveikos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Šinkūnas Tomas",
   "Occupation": "mokslo darbuotojas",
   "Phone": 9428,
   "Room": "V341",
   "Email0": "sinkunas@ibt.lt",
   "Email1": "tomas.sinkunas@bti.vu.lt",
   "Skyrius": "Baltymų-nukleorūgščių sąveikos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Karvelis Tautvydas",
   "Occupation": "mokslo darbuotojas",
   "Phone": 9428,
   "Room": "V341",
   "Email0": "tautvydas.karvelis@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Baltymų-nukleorūgščių sąveikos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Kostiuk Georgij",
   "Occupation": "mokslo darbuotojas",
   "Phone": 9426,
   "Room": "V337",
   "Email0": "georgij.kostiuk@ibt.lt",
   "Email1": "georgij.kostiuk@bti.vu.lt",
   "Skyrius": "Baltymų-nukleorūgščių sąveikos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Toliušis Paulius",
   "Occupation": "jaunesnysis mokslo darbuotojas",
   "Phone": 9448,
   "Room": "V343",
   "Email0": "paulius.toliusis@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Baltymų-nukleorūgščių sąveikos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Songailienė Inga",
   "Occupation": "jaunesnioji mokslo darbuotoja",
   "Phone": 9448,
   "Room": "V343",
   "Email0": "inga.songailiene@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Baltymų-nukleorūgščių sąveikos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Zagorskaitė Evelina",
   "Occupation": "jaunesnioji mokslo darbuotoja",
   "Phone": 9448,
   "Room": "V343",
   "Email0": "evelina.zagorskaite@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Baltymų-nukleorūgščių sąveikos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Merkys Andrius",
   "Occupation": "jaunesnysis mokslo darbuotojas",
   "Phone": 9423,
   "Room": "V325",
   "Email0": "andrius.merkys@gmail.com",
   "Email1": "",
   "Skyrius": "Baltymų-nukleorūgščių sąveikos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Bigelytė Greta",
   "Occupation": "jaunesnioji mokslo darbuotoja",
   "Phone": 9426,
   "Room": "V337",
   "Email0": "",
   "Email1": "",
   "Skyrius": "Baltymų-nukleorūgščių sąveikos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Vaitkus Antanas",
   "Occupation": "doktorantas",
   "Phone": 9423,
   "Room": "V325",
   "Email0": "",
   "Email1": "",
   "Skyrius": "Baltymų-nukleorūgščių sąveikos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Drabavičius Gediminas",
   "Occupation": "jaunesnysis mokslo darbuotojas",
   "Phone": 9427,
   "Room": "V339",
   "Email0": "gdrabavicius@gmail.com",
   "Email1": "",
   "Skyrius": "Baltymų-nukleorūgščių sąveikos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Grybauskas Algirdas",
   "Occupation": "doktorantas",
   "Phone": null,
   "Room": "",
   "Email0": "algirdas.grybauskas@yahoo.com",
   "Email1": "",
   "Skyrius": "Baltymų-nukleorūgščių sąveikos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Golovinas Edvardas",
   "Occupation": "doktorantas",
   "Phone": 9427,
   "Room": "V339",
   "Email0": "",
   "Email1": "",
   "Skyrius": "Baltymų-nukleorūgščių sąveikos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Irmantas Mogila",
   "Occupation": "doktorantas",
   "Phone": 9427,
   "Room": "V339",
   "Email0": "",
   "Email1": "",
   "Skyrius": "Baltymų-nukleorūgščių sąveikos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Dalia Smalakytė",
   "Occupation": "biologė tyrėja",
   "Phone": null,
   "Room": "V337",
   "Email0": "dalia.smalakyte@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Baltymų-nukleorūgščių sąveikos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Klimašauskas Saulius",
   "Occupation": "skyriaus vedėjas, vyriausiasis mokslo darbuotojas",
   "Phone": 2234350,
   "Room": "V303",
   "Email0": "klimasau@ibt.lt",
   "Email1": "saulius.klimasauskas@bti.vu.lt",
   "Skyrius": "DNR modifikacijos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Vilkaitis Giedrius",
   "Occupation": "vyriausiasis mokslo darbuotojas",
   "Phone": 2234372,
   "Room": "V305",
   "Email0": "giedrius@ibt.lt",
   "Email1": "giedrius.vilkaitis@bti.vu.lt",
   "Skyrius": "DNR modifikacijos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Kriukienė Edita",
   "Occupation": "vyresnioji mokslo darbuotoja",
   "Phone": 2234351,
   "Room": "V315",
   "Email0": "ediga@ibt.lt",
   "Email1": "edita.kriukiene@bti.vu.lt",
   "Skyrius": "DNR modifikacijos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Masevičius Viktoras",
   "Occupation": "vyriausiasis mokslo darbuotojas",
   "Phone": 9417,
   "Room": "C301",
   "Email0": "viktoras.masevicius@chf.vu.lt",
   "Email1": "",
   "Skyrius": "DNR modifikacijos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Rakauskaitė Rasa",
   "Occupation": "vyresnioji mokslo darbuotoja",
   "Phone": 4375,
   "Room": "C211",
   "Email0": "rasa.rakauskaite@bti.vu.lt",
   "Email1": "",
   "Skyrius": "DNR modifikacijos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Gordevičius Juozas",
   "Occupation": "vyresnysis mokslo darbuotojas",
   "Phone": 9455,
   "Room": "C542",
   "Email0": "juozas.gordevicius@gmc.vu.lt",
   "Email1": "",
   "Skyrius": "DNR modifikacijos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Tomkuvienė Miglė",
   "Occupation": "mokslo darbuotoja",
   "Phone": 9419,
   "Room": "V313",
   "Email0": "migleg@ibt.lt",
   "Email1": "migle.tomkuviene@bti.vu.lt",
   "Skyrius": "DNR modifikacijos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Stankevičius Vaidotas",
   "Occupation": "mokslo darbuotojas",
   "Phone": null,
   "Room": "",
   "Email0": "vaidotas.stankevicius@cr.vu.lt",
   "Email1": "",
   "Skyrius": "DNR modifikacijos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Osipenko Aleksandr",
   "Occupation": "jaunesnysis mokslo darbuotojas",
   "Phone": 2234373,
   "Room": "V307",
   "Email0": "aleksandr.osipenko@bti.vu.lt",
   "Email1": "",
   "Skyrius": "DNR modifikacijos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Staševskij Zdislav",
   "Occupation": "jaunesnysis mokslo darbuotojas",
   "Phone": 2234352,
   "Room": "C217",
   "Email0": "zdislav@ibt.lt",
   "Email1": "zdislav.stasevskij@bti.vu.lt",
   "Skyrius": "DNR modifikacijos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Urbanavičiūtė Giedrė",
   "Occupation": "jaunesnioji mokslo darbuotoja",
   "Phone": 4375,
   "Room": "C211",
   "Email0": "giedreu@ibt.lt",
   "Email1": "giedre.urbanaviciute@bti.vu.lt",
   "Skyrius": "DNR modifikacijos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Rukšėnaitė Audronė",
   "Occupation": "chemikė tyrėja",
   "Phone": 2234373,
   "Room": "V317",
   "Email0": "audrone.ruksenaite@bti.vu.lt",
   "Email1": "",
   "Skyrius": "DNR modifikacijos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Gasiulė Stasė",
   "Occupation": "jaunesnioji mokslo darbuotoja",
   "Phone": 9418,
   "Room": "V311",
   "Email0": "stase.butkyte@bti.vu.lt",
   "Email1": "",
   "Skyrius": "DNR modifikacijos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Ličytė Janina",
   "Occupation": "jaunesnioji mokslo darbuotoja",
   "Phone": 9419,
   "Room": "V313",
   "Email0": "janina.licyte@bti.vu.lt",
   "Email1": "",
   "Skyrius": "DNR modifikacijos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Mickutė Milda",
   "Occupation": "jaunesnioji mokslo darbuotoja",
   "Phone": 9418,
   "Room": "V311",
   "Email0": "milda.mickute@bti.vu.lt",
   "Email1": "",
   "Skyrius": "DNR modifikacijos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Gibas Povilas",
   "Occupation": "doktorantas",
   "Phone": 9455,
   "Room": "C542",
   "Email0": "povilas.gibas@bti.vu.lt",
   "Email1": "",
   "Skyrius": "DNR modifikacijos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Narmontė Milda",
   "Occupation": "doktorantė",
   "Phone": 2234352,
   "Room": "C217",
   "Email0": "milda.narmonte@gmc.vu.lt",
   "Email1": "",
   "Skyrius": "DNR modifikacijos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Kvederavičiūtė Kotryna",
   "Occupation": "jaunesnioji mokslo darbuotoja",
   "Phone": 9455,
   "Room": "C542",
   "Email0": "kotryna@ibt.lt",
   "Email1": "",
   "Skyrius": "DNR modifikacijos tyrimų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Eukariotų genų inžinerijos skyrius",
   "Occupation": "",
   "Phone": null,
   "Room": "",
   "Email0": "",
   "Email1": "",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Petraitytė-Burneikienė Rasa",
   "Occupation": "skyriaus vedėja, vyresnioji mokslo darbuotoja",
   "Phone": 22343599430,
   "Room": "C451",
   "Email0": "prasa@ibt.lt",
   "Email1": "rasa.burneikiene@bti.vu.lt",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Sasnauskas Kęstutis",
   "Occupation": "vyriausiasis mokslo darbuotojas",
   "Phone": 2234358,
   "Room": "V447",
   "Email0": "sasnausk@ibt.lt",
   "Email1": "kestutis.sasnauskas@bti.vu.lt",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Gedvilaitė Alma",
   "Occupation": "Mikroskysčių technologijų sektorius",
   "Phone": 2234359,
   "Room": "C467",
   "Email0": "agedv@ibt.lt",
   "Email1": "alma.gedvilaite@bti.vu.lt",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Ražanskienė Aušra",
   "Occupation": "vyresnioji mokslo darbuotoja",
   "Phone": 9440,
   "Room": "V421",
   "Email0": "ausra@ibt.lt",
   "Email1": "ausra.razanskiene@bti.vu.lt",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Žvirblis Gintautas",
   "Occupation": "vyresnysis mokslo darbuotojas",
   "Phone": 2234358,
   "Room": "C461",
   "Email0": "zvirb@ibt.lt",
   "Email1": "gintautas.zvirblis@bti.vu.lt",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Slibinskas Rimantas",
   "Occupation": "vyriausiasis mokslo darbuotojas",
   "Phone": 9432,
   "Room": "C455",
   "Email0": "slibinsk@ibt.lt",
   "Email1": "rimantas.slibinskas@bti.vu.lt",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Čiplys Evaldas",
   "Occupation": "vyresnysis mokslo darbuotojas",
   "Phone": 9432,
   "Room": "C455",
   "Email0": "evaldas@ibt.lt",
   "Email1": "evaldas.ciplys@bti.vu.lt",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Ražanskas Raimundas",
   "Occupation": "mokslo darbuotojas",
   "Phone": 9439,
   "Room": "V419",
   "Email0": "raimis@ibt.lt",
   "Email1": "raimundas.razanskas@bti.vu.lt",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Kazanavičiūtė Vaiva",
   "Occupation": "mokslo darbuotoja",
   "Phone": 9439,
   "Room": "V419",
   "Email0": "vaiva@ibt.lt",
   "Email1": "vaiva.kazanaviciute@bti.vu.lt",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Abraitienė Asta",
   "Occupation": "jaunesnioji mokslo darbuotoja",
   "Phone": 2234370,
   "Room": "V417",
   "Email0": "abraitiene@ibt.lt",
   "Email1": "asta.abraitiene@bti.vu.lt",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Bulavaitė Aistė",
   "Occupation": "mokslo darbuotoja",
   "Phone": 9431,
   "Room": "C453",
   "Email0": "aisteb@ibt.lt",
   "Email1": "aiste.bulavaite@bti.vu.lt",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Zaveckas Mindaugas",
   "Occupation": "mokslo darbuotojas",
   "Phone": 9431,
   "Room": "C453",
   "Email0": "mzaveck@ibt.lt",
   "Email1": "mindaugas.zaveckas@bti.vu.lt",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Žiogienė Danguolė",
   "Occupation": "mokslo darbuotoja",
   "Phone": 9434,
   "Room": "C463",
   "Email0": "danguole.ziogiene@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Tamošiūnas Paulius Lukas",
   "Occupation": "mokslo darbuotojas",
   "Phone": 2234359,
   "Room": "C451",
   "Email0": "paulius.tamosiunas@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Lazutka Justas",
   "Occupation": "jaunesnysis mokslo darbuotojas",
   "Phone": 2234359,
   "Room": "C451",
   "Email0": "justas.lazutka@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Rudokienė Eglutė",
   "Occupation": "chemikė tyrėja",
   "Phone": 2234370,
   "Room": "V417",
   "Email0": "egru@ibt.lt",
   "Email1": "eglute.rudokiene@bti.vu.lt",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Valavičiūtė Monika",
   "Occupation": "doktorantė",
   "Phone": 9434,
   "Room": "C463",
   "Email0": "monika.valaviciute@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Norkienė Milda",
   "Occupation": "mokslo darbuotoja",
   "Phone": 9434,
   "Room": "C463",
   "Email0": "milda@ibt.lt",
   "Email1": "milda.norkiene@bti.vu.lt",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Mickienė Gitana",
   "Occupation": "doktorantė",
   "Phone": 9433,
   "Room": "C457",
   "Email0": "gitana.zvirblyte@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Bakūnaitė Edita",
   "Occupation": "doktorantė",
   "Phone": 9429,
   "Room": "C449",
   "Email0": "edita.bakunaite@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Starkevič Urtė",
   "Occupation": "doktorantė",
   "Phone": 9439,
   "Room": "V419",
   "Email0": "urte.starkevic@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Zinkevičiūtė Rūta",
   "Occupation": "doktorantė",
   "Phone": 9429,
   "Room": "C449",
   "Email0": "ruta.zinkeviciute@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Dapkūnas Žilvinas",
   "Occupation": "doktorantas",
   "Phone": 9435,
   "Room": "C465",
   "Email0": "zilvinas.dapkunas@gmail.com",
   "Email1": "",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Paškevičius Šarūnas",
   "Occupation": "doktorantas",
   "Phone": 9433,
   "Room": "C457",
   "Email0": "paskevicius.sarunas@gmail.com",
   "Email1": "",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Macijauskaitė Neringa",
   "Occupation": "doktorantė",
   "Phone": 9429,
   "Room": "C449",
   "Email0": "neringamacij@gmail.com",
   "Email1": "",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Žitkus Eimantas",
   "Occupation": "doktorantas",
   "Phone": 9432,
   "Room": "C455",
   "Email0": "eimantas.zitkus@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Špakova Aliona",
   "Occupation": "doktorantė",
   "Phone": 9431,
   "Room": "C453",
   "Email0": "spakova.aliona@gmail.com",
   "Email1": "aliona.spakova@bti.vu.lt",
   "Skyrius": "Eukariotų genų inžinerijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Mažutis Linas",
   "Occupation": "vyresnysis mokslo darbuotojas",
   "Phone": 2234356,
   "Room": "C225",
   "Email0": "linas.mazutis@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Mikroskysčių technologijų sektorius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Milkus Valdemaras",
   "Occupation": "chemikas tyrėjas",
   "Phone": 9402,
   "Room": "C223",
   "Email0": "valdemaras.milkus@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Mikroskysčių technologijų sektorius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Žilionis Rapolas",
   "Occupation": "doktorantas",
   "Phone": 9400,
   "Room": "C219",
   "Email0": "rapolas.zilionis@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Mikroskysčių technologijų sektorius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Leonavičius Karolis",
   "Occupation": "biologas tyrėjas",
   "Phone": null,
   "Room": "",
   "Email0": "",
   "Email1": "",
   "Skyrius": "Mikroskysčių technologijų sektorius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Kučiauskas Dalius",
   "Occupation": "biologas tyrėjas",
   "Phone": 9401,
   "Room": "C221",
   "Email0": "",
   "Email1": "",
   "Skyrius": "Mikroskysčių technologijų sektorius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Nainys Juozas",
   "Occupation": "doktorantas",
   "Phone": 9400,
   "Room": "C219",
   "Email0": "juozas.nainys@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Mikroskysčių technologijų sektorius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Goda Karolis",
   "Occupation": "jaunesnysis mokslo darbuotojas",
   "Phone": 9401,
   "Room": "C221",
   "Email0": "",
   "Email1": "",
   "Skyrius": "Mikroskysčių technologijų sektorius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Matulis Daumantas",
   "Occupation": "skyriaus vedėjas, vyriausiasis mokslo darbuotojas",
   "Phone": 2234364,
   "Room": "C309",
   "Email0": "matulis@ibt.lt",
   "Email1": "daumantas.matulis@bti.vu.lt",
   "Skyrius": "Biotermodinamikos ir vaistų tyrimo skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Zubrienė Asta",
   "Occupation": "vyresnioji mokslo darbuotoja",
   "Phone": 9412,
   "Room": "C319",
   "Email0": "astzu@ibt.lt",
   "Email1": "asta.zubriene@bti.vu.lt",
   "Skyrius": "Biotermodinamikos ir vaistų tyrimo skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Petrauskas Vytautas",
   "Occupation": "vyresnysis mokslo darbuotojas",
   "Phone": 9408,
   "Room": "C307",
   "Email0": "v.petrauskas@ibt.lt",
   "Email1": "vytautas.petrauskas@bti.vu.lt",
   "Skyrius": "Biotermodinamikos ir vaistų tyrimo skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Smirnovas Vytautas",
   "Occupation": "vyriausiasis mokslo darbuotojas",
   "Phone": 22343629420,
   "Room": "C303",
   "Email0": "vytautas.smirnovas@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Biotermodinamikos ir vaistų tyrimo skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Baranauskienė Lina",
   "Occupation": "vyresnioji mokslo darbuotoja",
   "Phone": 9412,
   "Room": "C319",
   "Email0": "linami@ibt.lt",
   "Email1": "lina.baranauskiene@bti.vu.lt",
   "Skyrius": "Biotermodinamikos ir vaistų tyrimo skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Dudutienė Virginija",
   "Occupation": "vyresnioji mokslo darbuotoja",
   "Phone": 9413,
   "Room": "C321",
   "Email0": "virginija@ibt.lt",
   "Email1": "virginija.dudutiene@bti.vu.lt",
   "Skyrius": "Biotermodinamikos ir vaistų tyrimo skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Čapkauskaitė Edita",
   "Occupation": "vyresnioji mokslo darbuotoja",
   "Phone": 9413,
   "Room": "C321",
   "Email0": "edita.capkauskaite@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Biotermodinamikos ir vaistų tyrimo skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Petrikaitė Vilma",
   "Occupation": "vyresnioji mokslo darbuotoja",
   "Phone": 9416,
   "Room": "C331",
   "Email0": "vilma@ibt.lt",
   "Email1": "vilma.petrikaite@bti.vu.lt",
   "Skyrius": "Biotermodinamikos ir vaistų tyrimo skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Juozapaitienė Vaida",
   "Occupation": "mokslo darbuotoja",
   "Phone": 9409,
   "Room": "C311",
   "Email0": "jogaite@ibt.lt",
   "Email1": " vaida.jogaite@bti.vu.lt",
   "Skyrius": "Biotermodinamikos ir vaistų tyrimo skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Kazlauskas Egidijus",
   "Occupation": "mokslo darbuotojas",
   "Phone": 9412,
   "Room": "C319",
   "Email0": "ekazl@ibt.lt",
   "Email1": "egidijus.kazlauskas@bti.vu.lt",
   "Skyrius": "Biotermodinamikos ir vaistų tyrimo skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Grincevičienė Švitrigailė",
   "Occupation": "mokslo darbuotoja",
   "Phone": 9410,
   "Room": "C315",
   "Email0": "svitrigaile@gmail.com",
   "Email1": "",
   "Skyrius": "Biotermodinamikos ir vaistų tyrimo skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Jachno Jelena",
   "Occupation": "jaunesnioji mokslo darbuotoja",
   "Phone": 9409,
   "Room": "C311",
   "Email0": "elper@ibt.lt",
   "Email1": "jelena.jachno@bti.vu.lt",
   "Skyrius": "Biotermodinamikos ir vaistų tyrimo skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Michailovienė Vilma",
   "Occupation": "jaunesnioji mokslo darbuotoja",
   "Phone": 9411,
   "Room": "C317",
   "Email0": "roze@ibt.lt",
   "Email1": "vilma.michailoviene@bti.vu.lt",
   "Skyrius": "Biotermodinamikos ir vaistų tyrimo skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Zakšauskas Audrius",
   "Occupation": "jaunesnysis mokslo darbuotojas",
   "Phone": 9413,
   "Room": "C321",
   "Email0": "audrius.zaksauskas@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Biotermodinamikos ir vaistų tyrimo skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Lingė Darius",
   "Occupation": "informacinių sistemų inžinierius",
   "Phone": 9411,
   "Room": "C317",
   "Email0": "degama@ibt.lt",
   "Email1": "darius.linge@bti.vu.lt",
   "Skyrius": "Biotermodinamikos ir vaistų tyrimo skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Smirnov Alexey",
   "Occupation": "biologas tyrėjas",
   "Phone": 9424,
   "Room": "V327",
   "Email0": "alexeyus1@gmail.com",
   "Email1": "",
   "Skyrius": "Biotermodinamikos ir vaistų tyrimo skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Skvarnavičius Gediminas",
   "Occupation": "jaunesnysis mokslo darbuotojas",
   "Phone": 9416,
   "Room": "C331",
   "Email0": "",
   "Email1": "",
   "Skyrius": "Biotermodinamikos ir vaistų tyrimo skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Kazokaitė Justina",
   "Occupation": "doktorantė",
   "Phone": 9416,
   "Room": "C331",
   "Email0": "kazokaite@ibt.lt",
   "Email1": "justina.kazokaite@bti.vu.lt",
   "Skyrius": "Biotermodinamikos ir vaistų tyrimo skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Janonienė Agnė",
   "Occupation": "doktorantė",
   "Phone": 9416,
   "Room": "C331",
   "Email0": "agne.vegyte@gmail.com",
   "Email1": "",
   "Skyrius": "Biotermodinamikos ir vaistų tyrimo skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Šneideris Tomas",
   "Occupation": "doktorantas",
   "Phone": 9407,
   "Room": "C305",
   "Email0": "sneideris.t@gmail.com",
   "Email1": "tomas.sneideris@bti.vu.lt",
   "Skyrius": "Biotermodinamikos ir vaistų tyrimo skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Smirnovienė Joana",
   "Occupation": "jaunesnioji mokslo darbuotoja",
   "Phone": 9408,
   "Room": "C307",
   "Email0": "",
   "Email1": "",
   "Skyrius": "Biotermodinamikos ir vaistų tyrimo skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Matijošytė Inga",
   "Occupation": "vyresnioji mokslo darbuotoja",
   "Phone": 2234371,
   "Room": "C325",
   "Email0": "minga@ibt.lt",
   "Email1": " inga.matijosyte@bti.vu.lt",
   "Skyrius": "Taikomosios biokatalizės sektorius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Šiekštelė Rimantas",
   "Occupation": "jaunesnysis mokslo darbuotojas",
   "Phone": 9415,
   "Room": "C329",
   "Email0": "sieksta@ibt.lt",
   "Email1": "rimantas.siekstele@bti.vu.lt",
   "Skyrius": "Taikomosios biokatalizės sektorius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Veteikytė Aušra",
   "Occupation": "biologė tyrėja",
   "Phone": 9414,
   "Room": "C323",
   "Email0": "ausra.veteikyte@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Taikomosios biokatalizės sektorius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Matikevičienė Veslava",
   "Occupation": "doktorantė",
   "Phone": 2234371,
   "Room": "C325",
   "Email0": "veslava@gmail.com",
   "Email1": "",
   "Skyrius": "Taikomosios biokatalizės sektorius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Žvirblienė Aurelija",
   "Occupation": "skyriaus vedėja, vyriausioji mokslo darbuotoja",
   "Phone": 2234360,
   "Room": "V231",
   "Email0": "azvirb@ibt.lt",
   "Email1": "aurelija.zvirbliene@bti.vu.lt",
   "Skyrius": "Imunologijos ir ląstelės biologijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Plečkaitytė Milda",
   "Occupation": "vyriausioji mokslo darbuotoja",
   "Phone": 9436,
   "Room": "C465",
   "Email0": "mildap@ibt.lt",
   "Email1": "milda.pleckaityte@bti.vu.lt",
   "Skyrius": "Imunologijos ir ląstelės biologijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Kanopka Arvydas",
   "Occupation": "vyresnysis mokslo darbuotojas",
   "Phone": 2234369,
   "Room": "V411",
   "Email0": "kanopka@ibt.lt",
   "Email1": "arvydas.kanopka@bti.vu.lt",
   "Skyrius": "Imunologijos ir ląstelės biologijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Kučinskaitė-Kodzė Indrė",
   "Occupation": "vyresnioji mokslo darbuotoja",
   "Phone": 9403,
   "Room": "V225",
   "Email0": "indkuc@ibt.lt",
   "Email1": "indre.kodze@bti.vu.lt",
   "Skyrius": "Imunologijos ir ląstelės biologijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Matulienė Jurgita",
   "Occupation": "vyresnioji mokslo darbuotoja",
   "Phone": 9409,
   "Room": "C311",
   "Email0": "matuliene@ibt.lt",
   "Email1": "jurgita.matuliene@bti.vu.lt",
   "Skyrius": "Imunologijos ir ląstelės biologijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Stakėnas Petras",
   "Occupation": "mokslo darbuotojas",
   "Phone": 2234369,
   "Room": "V411",
   "Email0": "pstak@ibt.lt",
   "Email1": "petras.stakenas@bti.vu.lt",
   "Skyrius": "Imunologijos ir ląstelės biologijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Jakubauskienė Eglė",
   "Occupation": "mokslo darbuotoja",
   "Phone": 9437,
   "Room": "V413",
   "Email0": "egle.jakubauskiene@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Imunologijos ir ląstelės biologijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Pečiulienė Inga",
   "Occupation": "jaunesnioji mokslo darbuotoja",
   "Phone": 9437,
   "Room": "V413",
   "Email0": "peciuliene@ibt.lt",
   "Email1": "inga.peciuliene@bti.vu.lt",
   "Skyrius": "Imunologijos ir ląstelės biologijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Simanavičius Martynas",
   "Occupation": "doktorantas",
   "Phone": 9403,
   "Room": "V225",
   "Email0": "martynas.simanavicius@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Imunologijos ir ląstelės biologijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Bakonytė Daiva",
   "Occupation": "biologė tyrėja",
   "Phone": 2234369,
   "Room": "V411",
   "Email0": "daiva@ibt.lt",
   "Email1": "daiva.bakonyte@bti.vu.lt",
   "Skyrius": "Imunologijos ir ląstelės biologijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Dalgėdienė Indrė",
   "Occupation": "jaunesnioji mokslo darbuotoja",
   "Phone": 9405,
   "Room": "V229",
   "Email0": "indre@ibt.lt",
   "Email1": "indre.dalgediene@bti.vu.lt",
   "Skyrius": "Imunologijos ir ląstelės biologijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Stravinskienė Dovilė",
   "Occupation": "doktorantė",
   "Phone": 9404,
   "Room": "V227",
   "Email0": "dovile.dekaminaviciute@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Imunologijos ir ląstelės biologijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Vilys Laurynas",
   "Occupation": "jaunesnysis mokslo darbuotojas",
   "Phone": 9437,
   "Room": "V413",
   "Email0": "laurynas@ibt.lt",
   "Email1": "laurynas.vilys@bti.vu.lt",
   "Skyrius": "Imunologijos ir ląstelės biologijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Janulaitienė Miglė",
   "Occupation": "jaunesnioji mokslo darbuotoja",
   "Phone": 9436,
   "Room": "C469",
   "Email0": "migle.j@gmail.com",
   "Email1": "",
   "Skyrius": "Imunologijos ir ląstelės biologijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Zilnytė Milda",
   "Occupation": "jaunesnioji mokslo darbuotoja",
   "Phone": 9405,
   "Room": "V229",
   "Email0": "milda.zilnyte@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Imunologijos ir ląstelės biologijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Rubinaitė Vilija",
   "Occupation": "doktorantė",
   "Phone": null,
   "Room": "",
   "Email0": "vilija.rubin@yahoo.com",
   "Email1": "",
   "Skyrius": "Imunologijos ir ląstelės biologijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Asta Lučiūnaitė",
   "Occupation": "doktorantė",
   "Phone": 9405,
   "Room": "V229",
   "Email0": "asta.luciunaite@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Imunologijos ir ląstelės biologijos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Venclovas Česlovas",
   "Occupation": "skyriaus vedėjas, vyriausiasis mokslo darbuotojas",
   "Phone": 2234368,
   "Room": "C535",
   "Email0": "venclovas@ibt.lt",
   "Email1": "ceslovas.venclovas@bti.vu.lt",
   "Skyrius": "Bioinformatikos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Margelevičius Mindaugas",
   "Occupation": "vyresnysis mokslo darbuotojas",
   "Phone": 9442,
   "Room": "C528",
   "Email0": "minmar@ibt.lt",
   "Email1": "mindaugas.margelevicius@bti.vu.lt",
   "Skyrius": "Bioinformatikos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Kairys Visvaldas",
   "Occupation": "vyresnysis mokslo darbuotojas",
   "Phone": 9441,
   "Room": "C527",
   "Email0": "visvaldas.kairys@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Bioinformatikos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Timinskas Albertas",
   "Occupation": "mokslo darbuotojas",
   "Phone": 9445,
   "Room": "C532",
   "Email0": "timis@ibt.lt",
   "Email1": "albertas.timinskas@bti.vu.lt",
   "Skyrius": "Bioinformatikos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Kazlauskas Darius",
   "Occupation": "mokslo darbuotojas",
   "Phone": 9444,
   "Room": "C531",
   "Email0": "d.kazlauskas@ibt.lt",
   "Email1": "darius.kazlauskas@bti.vu.lt",
   "Skyrius": "Bioinformatikos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Dapkūnas Justas",
   "Occupation": "mokslo darbuotojas",
   "Phone": 9446,
   "Room": "C533",
   "Email0": "justas.dapkunas@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Bioinformatikos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Dičiūnas Rytis",
   "Occupation": "inžinierius tyrėjas",
   "Phone": 9443,
   "Room": "C529",
   "Email0": "rytis@ibt.lt",
   "Email1": "rytis.diciunas@bti.vu.lt",
   "Skyrius": "Bioinformatikos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Olechnovič Kliment",
   "Occupation": "mokslo darbuotojas",
   "Phone": 9447,
   "Room": "C534",
   "Email0": "kliment@ibt.lt",
   "Email1": "kliment.olechnovic@bti.vu.lt",
   "Skyrius": "Bioinformatikos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Timinskas Kęstutis",
   "Occupation": "inžinierius tyrėjas",
   "Phone": 9447,
   "Room": "C534",
   "Email0": "kestutis.timinskas@bti.vu.lt",
   "Email1": "",
   "Skyrius": "Bioinformatikos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Aurimas Nausėdas",
   "Occupation": "doktorantas",
   "Phone": 9442,
   "Room": "C528",
   "Email0": "aurimas.nausedas@gmc.vu.lt",
   "Email1": "aurimas.nausedas@bti.stud.vu.lt",
   "Skyrius": "Bioinformatikos skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Petronis Artūras",
   "Occupation": "vyriausiasis mokslo darbuotojas",
   "Phone": null,
   "Room": "",
   "Email0": "art.petronis@camh.ca",
   "Email1": "",
   "Skyrius": "Epigenominių tyrimų laboratorija",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Kriščiūnas Algimantas",
   "Occupation": "jaunesnysis mokslo darbuotojas",
   "Phone": 9455,
   "Room": "C542",
   "Email0": "",
   "Email1": "",
   "Skyrius": "Epigenominių tyrimų laboratorija",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Koncevičius Karolis",
   "Occupation": "jaunesnysisi mokslo darbuotojas",
   "Phone": 9455,
   "Room": "C542",
   "Email0": "",
   "Email1": "",
   "Skyrius": "Epigenominių tyrimų laboratorija",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Žiūkaitė Janina",
   "Occupation": "vyriausioji specialistė",
   "Phone": 22343659463,
   "Room": "C526",
   "Email0": "ziuk@ibt.lt",
   "Email1": "janina.ziukaite@bti.vu.lt",
   "Skyrius": "Bendrųjų reikalų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Jachno Jelena",
   "Occupation": "vyriausioji specialistė",
   "Phone": 9409,
   "Room": "C311",
   "Email0": "elper@ibt.lt",
   "Email1": "jelena.jachno@bti.vu.lt",
   "Skyrius": "Bendrųjų reikalų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Sukackas Martynas",
   "Occupation": "informacinių sistemų inžinierius",
   "Phone": null,
   "Room": "C524",
   "Email0": "martynas@ibt.lt",
   "Email1": "martynas.sukackas@bti.vu.lt",
   "Skyrius": "Bendrųjų reikalų skyrius",
   "Institute": "Instute of Biotechnology"
 },
 {
   "Name": "Krutkis Algimantas",
   "Occupation": "inžinierius tyrėjas",
   "Phone": null,
   "Room": "",
   "Email0": "algimantas.krutkis@bchi.vu.lt",
   "Email1": "",
   "Skyrius": "Bendrųjų reikalų skyrius",
   "Institute": "Instute of Biotechnology"
 }
]