import React, { Component } from 'react';
import './App.css';
import SearchAppBar from './SearchAppBar';
import TemporaryDrawer from './TemporaryDrawer';
import pubmedSearch from './PubmedSearch';
import CardList from './CardList';
import CircularProgress from '@material-ui/core/CircularProgress';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import ReactGA from 'react-ga';

ReactGA.initialize('UA-145080373-1');
ReactGA.pageview(window.location.pathname + window.location.search);


const theme = createMuiTheme({
    palette: {
      primary: {
        main: '#880E4F'
      },
      secondary: {
        main: '#f44336',
      },
    },
  });

class App extends Component {

  constructor() {
    super();
    this.state = { 
      isLoading: false, 
      isDrawerOpen: false, 
      title: "People search", 
      searchResults: undefined
    };
  }
  onMenuClick() {
    this.setState({ isDrawerOpen: !this.state.isDrawerOpen });
  };

  async onSearchClick() {
    this.setState({isLoading: true})
    const response = await pubmedSearch.getData(this.state.searchText)
    this.setState({searchResults: response, isLoading: false});
    //console.log("response", response)
  }

  onSearchChange(event) {
    this.setState({ searchText: event.target.value });
  }

  onListItemClick(text) {
    console.log("onListItemClick")
    this.setState({ title: text });
  }

  
  render() {
    return (
      <ThemeProvider theme={theme}>
        <div className="App">
          <SearchAppBar 
            onMenuClick={() => this.onMenuClick()} 
            onSearchClick={() => this.onSearchClick()} 
            onSearchChange={(event) => this.onSearchChange(event)}
            title={this.state.title} 
          />
          <TemporaryDrawer 
            onMenuClick={() => this.onMenuClick()} 
            isDrawerOpen={this.state.isDrawerOpen} 
            onListItemClick={(text) => this.onListItemClick(text)}
          />
          {this.state.isLoading 
            ? <CircularProgress style = { {
                  position: 'absolute',
                  left: '50%',
                  top: '35%',
                }}/>
            : <CardList responseArray={this.state.searchResults}/>
          }
        </div>
      </ThemeProvider>
    );
  }

}

export default App;
