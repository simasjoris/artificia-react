const axios = require('axios');
const parseString = require('xml2js').parseString;
const {promisify} = require('util');

const employees = require('./institutesFull')

const BASE_URL = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/'
const FETCH = 'efetch.fcgi'
const SEARCH = 'esearch.fcgi'

const idsRequestUrl = `${BASE_URL}${SEARCH}`

const abstractsRequestUrl = `${BASE_URL}${FETCH}`

const parseXML = promisify(parseString)

const filterAuthors = (searchResults) => {
    //console.log("search results ", searchResults)
    return employees.filter(employee => {
        //console.log("employee", employee.Name)
        return searchResults.includes(employee.Name)
    })
}


const getPublicationIds = async (searchTerm) => {
    try {
      const response = await axios.get(idsRequestUrl, {
        params: {
            db: 'pubmed',
            retmode: 'json',
            retmax: '100',
            sort: 'relevance',
            term: `(${searchTerm} Institute of Biotechnology Vilnius) or (${searchTerm} of Biosciences Vilnius) or (${searchTerm} Institute of Biochemistry Vilnius)`
        }
      });
      const idList = response.data.esearchresult.idlist;
      return idList;
    } catch (error) {
      console.error(error);
    }
}

const getPublicationAbstracts = async (ids) => {
    try {
        const response = await axios.get(abstractsRequestUrl, {
          params: {
              db: 'pubmed',
              retmode: 'xml',
              id: ids.toString()
          }
        });
        const jsdata = await parseXML(response.data)
        if(!jsdata.PubmedArticleSet)
            return [];
        const articles = jsdata.PubmedArticleSet.PubmedArticle;
        const authors = [];
         articles.forEach(article => {
            const articleAuthors = article.MedlineCitation[0].Article[0].AuthorList[0].Author
            Array.prototype.push.apply(authors, getAuthorStringList(articleAuthors))
        });
        const uniqueAuthors = [...new Set(authors)];
        const filteredAuthors = filterAuthors(uniqueAuthors);
        //console.log("filteredAuthors", filteredAuthors.length)
        return filteredAuthors
            .slice(0, 100)
    } catch (error) {
        console.error(error);
      }
}

const getAuthorStringList = (authorList) => {
    return authorList.map(author => getAuthorString(author))
}

const getAuthorString = (author) => {
    return `${author.LastName} ${author.ForeName}`
}

const getData = async (searchTerm) => {
    const idList = await getPublicationIds(searchTerm);
    const abstracts = await getPublicationAbstracts(idList);
    //console.log(abstracts)
    return abstracts;
}

export default {
    getPublicationIds,
    getPublicationAbstracts,
    getData
}
