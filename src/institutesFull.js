module.exports = [
 {
   "Phone": "223 4350",
   "Email0": "saulius.klimasauskas@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV303 kab.",
   "Name": "Klimašauskas Saulius",
   "Occupation": "Habil. dr., Direktorius, Vyriausiasis mokslo darbuotojas, Vyriausiasis mokslo darbuotojas (su išskirtinio profesoriaus kategorija)",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9420",
   "Email0": "vytautas.smirnovas@bti.vu.lt\nweb.vu.lt/bti/v.smirnovas",
   "Room": "Saulėtekio al. 7, Vilnius\nC303 kab.",
   "Name": "Smirnovas Vytautas",
   "Occupation": "Dr., Direktoriaus pavaduotojas, Vyriausiasis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4377",
   "Email0": "asta.abraitiene@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC504 kab.",
   "Name": "Abraitienė Asta",
   "Occupation": "Dr., Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "simonas.asmontas@gmc.vu.lt",
   "Room": "",
   "Name": "Ašmontas Simonas",
   "Occupation": "Specialistas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "justinas.babinskas@gmc.vu.lt",
   "Room": "Saulėtekio al. 7\nC323 kab.",
   "Name": "Babinskas Justinas",
   "Occupation": "Laborantas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4369",
   "Email0": "daiva.bakonyte@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV411 kab.",
   "Name": "Bakonytė Daiva",
   "Occupation": "Biologė tyrėja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9429",
   "Email0": "edita.bakunaite@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC449 kab.",
   "Name": "Bakūnaitė Edita",
   "Occupation": "Jaunesnioji mokslo darbuotoja, Doktorantė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9412",
   "Email0": "lina.baranauskiene@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC319 kab.",
   "Name": "Baranauskienė Lina",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja, Mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "tadas.bareikis@gmc.vu.lt",
   "Room": "",
   "Name": "Bareikis Tadas",
   "Occupation": "Laborantas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "vilte.bautronyte@gmc.vu.lt",
   "Room": "",
   "Name": "Bautronytė Viltė",
   "Occupation": "Specialistė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "alina.belova@gmc.vu.lt",
   "Room": "",
   "Name": "Belova Alina",
   "Occupation": "Laborantė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "greta.bigelyte@bti.vu.lt",
   "Room": "",
   "Name": "Bigelytė Greta",
   "Occupation": "Jaunesnioji mokslo darbuotoja, Laborantė, Doktorantė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4365 9462",
   "Email0": "leonora.bilinskiene@gmc.vu.lt",
   "Room": "Vilnius, Saulėtekio al.,7\nC525 kab.",
   "Name": "Bilinskienė Leonora",
   "Occupation": "Jaunesnioji specialistė viešiesiems pirkimams, Projekto administratorė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9431",
   "Email0": "aiste.bulavaite@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC453 kab.",
   "Name": "Bulavaitė Aistė",
   "Occupation": "Dr., Mokslo darbuotoja, Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9413",
   "Email0": "edita.capkauskaite@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC321 kab.",
   "Name": "Čapkauskaitė Edita",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4425",
   "Email0": "laimute.cekutiene@cr.vu.lt",
   "Room": "Saulėtekio al.7, Vilnius\nC513 kab.",
   "Name": "Čekutienė Laimutė",
   "Occupation": "Projekto viešųjų pirkimų specialistė, Viešųjų pirkimų specialistė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9432",
   "Email0": "evaldas.ciplys@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC455 kab.",
   "Name": "Čiplys Evaldas",
   "Occupation": "Dr., Vyresnysis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9405",
   "Email0": "indre.dalgediene@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius LT-10223\nV229 kab.",
   "Name": "Dalgėdienė Indrė",
   "Occupation": "Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9446",
   "Email0": "justas.dapkunas@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC533 kab.",
   "Name": "Dapkūnas Justas",
   "Occupation": "Dr., Mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "simonas.daunys@gmc.vu.lt",
   "Room": "",
   "Name": "Daunys Simonas",
   "Occupation": "Biologas tyrėjas, Doktorantas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9410",
   "Email0": "leokadija.davidian@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC315 kab.",
   "Name": "Davidian Leokadija",
   "Occupation": "Jaunesnioji laborantė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9443",
   "Email0": "rytis.diciunas@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC529 kab.",
   "Name": "Dičiūnas Rytis",
   "Occupation": "Inžinierius tyrėjas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "liodzia.digliene@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius",
   "Name": "Diglienė Liodzia",
   "Occupation": "Jaunesnioji laborantė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "gediminas.drabavicius@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV339 kab.",
   "Name": "Drabavičius Gediminas",
   "Occupation": "Jaunesnysis mokslo darbuotojas, Doktorantas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "rugile.drulyte@gmc.vu.lt",
   "Room": "",
   "Name": "Drulytė Rugilė",
   "Occupation": "Specialistė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9413",
   "Email0": "virginija.dudutiene@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC321 kab.",
   "Name": "Dudutienė Virginija",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja, Mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4365",
   "Email0": "vilma.gaseviciene@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC526 kab.",
   "Name": "Gasevičienė Vilma",
   "Occupation": "Sekretorė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9418",
   "Email0": "stase.gasiule@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV311 kab.",
   "Name": "Gasiulė Stasė",
   "Occupation": "Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "4443",
   "Email0": "giedrius.gasiunas@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV331 kab.",
   "Name": "Gasiūnas Giedrius",
   "Occupation": "Dr., Vyresnysis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9421",
   "Email0": "daiva.gedgaudiene@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV317 kab.",
   "Name": "Gedgaudienė Daiva",
   "Occupation": "Jaunesnioji laborantė, Patalpų tvarkytoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4359",
   "Email0": "alma.gedvilaite@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC467 kab.",
   "Name": "Gedvilaitė Alma",
   "Occupation": "Dr. (HP), Vyriausioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "emilis.gegevicius@gmc.vu.lt",
   "Room": "C219 kab.",
   "Name": "Gegevičius Emilis",
   "Occupation": "Biologas tyrėjas, Specialistas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "povilas.gibas@bti.vu.lt",
   "Room": "Saulėtekio al., 7, Vilnius\nC211 kab.",
   "Name": "Gibas Povilas",
   "Occupation": "Jaunesnysis mokslo darbuotojas, Inžinierius tyrėjas, Doktorantas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "karolis.goda@gmc.vu.lt",
   "Room": "",
   "Name": "Goda Karolis",
   "Occupation": "Jaunesnysis mokslo darbuotojas, Doktorantas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9427",
   "Email0": "edvardas.golovinas@gmc.vu.lt",
   "Room": "V339 kab.",
   "Name": "Golovinas Edvardas",
   "Occupation": "Biologas tyrėjas, Doktorantas, Laborantas, Specialistas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "210 9730",
   "Email0": "juozas.gordevicius@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius",
   "Name": "Gordevičius Juozas",
   "Occupation": "Dr., Vyresnysis mokslo darbuotojas, Mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "4353",
   "Email0": "saulius.grazulis@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV323 kab.",
   "Name": "Gražulis Saulius",
   "Occupation": "Dr., Vyriausiasis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "algirdas.grybauskas@gmc.vu.lt",
   "Room": "",
   "Name": "Grybauskas Algirdas",
   "Occupation": "Jaunesnysis mokslo darbuotojas, Doktorantas, Specialistas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "svitrigaile.grinceviciene@bti.vu.lt",
   "Room": "",
   "Name": "Grincevičienė Švitrigailė",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "diana.ikasalaite@gmc.vu.lt",
   "Room": "",
   "Name": "Ikasalaitė Diana",
   "Occupation": "Laborantė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "aiste.imbrasaite@gmc.vu.lt",
   "Room": "",
   "Name": "Imbrasaitė Aistė",
   "Occupation": "Biologė tyrėja, Doktorantė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9409",
   "Email0": "jelena.jachno@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC311 kab.",
   "Name": "Jachno Jelena",
   "Occupation": "Jaunesnioji mokslo darbuotoja, Viešųjų pirkimų administratorė, Specialistė darbų saugai",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9437",
   "Email0": "egle.jakubauskiene@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV413 kab.",
   "Name": "Jakubauskienė Eglė",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9433",
   "Email0": "olga.jancevskaja@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC457 kab.",
   "Name": "Jančevskaja Olga",
   "Occupation": "Jaunesnioji laborantė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4400",
   "Email0": "marija.jankunec@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC415 kab.",
   "Name": "Jankunec Marija",
   "Occupation": "Dr., Laborantė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9416",
   "Email0": "agne.vegyte@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC331 kab.",
   "Name": "Janonienė Agnė",
   "Occupation": "Jaunesnioji mokslo darbuotoja, Doktorantė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9436",
   "Email0": "migle.janulaitiene@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC469 kab.",
   "Name": "Janulaitienė Miglė",
   "Occupation": "Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9433",
   "Email0": "danute.jaskyte@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC457 kab.",
   "Name": "Jaskytė Danutė",
   "Occupation": "Jaunesnioji laborantė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9409",
   "Email0": "vaida.jogaite@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC311 kab.",
   "Name": "Juozapaitienė Vaida",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "260 2888",
   "Email0": "mindaugas.juozapaitis@gmc.vu.lt",
   "Room": "A.V. Graičiūno g. 8, Vilnius\n223 kab.",
   "Name": "Juozapaitis Mindaugas",
   "Occupation": "Dr., Vyresnysis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "jonas.juozapaitis@gmc.vu.lt",
   "Room": "",
   "Name": "Juozapaitis Jonas",
   "Occupation": "Biologas tyrėjas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "karolina.juskaite@gmc.vu.lt",
   "Room": "",
   "Name": "Juškaitė Karolina",
   "Occupation": "Biologė tyrėja, Specialistė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9441",
   "Email0": "visvaldas.kairys@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC527 kab.",
   "Name": "Kairys Visvaldas",
   "Occupation": "Dr. (HP), Vyresnysis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4369",
   "Email0": "arvydas.kanopka@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV411 kab.",
   "Name": "Kanopka Arvydas",
   "Occupation": "Dr., Vyresnysis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "laurynas.karpus@gmc.vu.lt",
   "Room": "",
   "Name": "Karpus Laurynas",
   "Occupation": "Biologas tyrėjas, Specialistas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "260 2111",
   "Email0": "tautvydas.karvelis@bti.vu.lt",
   "Room": "A.V. Graičiūno g. 8, LT-02241",
   "Name": "Karvelis Tautvydas",
   "Occupation": "Dr., Mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4411",
   "Email0": "algirdas.kaupinis@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV213 kab.",
   "Name": "Kaupinis Algirdas",
   "Occupation": "Dr., Mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "vaiva.kazanaviciute@bti.vu.lt",
   "Room": "",
   "Name": "Kazanavičiūtė Vaiva",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9459",
   "Email0": "darius.kazlauskas@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC531 kab.",
   "Name": "Kazlauskas Darius",
   "Occupation": "Dr., Mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9412",
   "Email0": "egidijus.kazlauskas@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC319 kab.",
   "Name": "Kazlauskas Egidijus",
   "Occupation": "Dr., Mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9416",
   "Email0": "justina.kazokaite@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC331 kab.",
   "Name": "Kazokaitė Justina",
   "Occupation": "Dr., Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "rytis.kisielius@gmc.vu.lt",
   "Room": "",
   "Name": "Kisielius Rytis",
   "Occupation": "Specialistas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "vaidas.klimkevicius@chf.vu.lt",
   "Room": "Naugarduko g. 24, Vilnius\n249 kab.",
   "Name": "Klimkevičius Vaidas",
   "Occupation": "Dr., Mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "210 9730",
   "Email0": "karolis.koncevicius@gmc.vu.lt",
   "Room": "Akademijos 4, Vilnius LT-08663\n612 kab.",
   "Name": "Koncevičius Karolis",
   "Occupation": "Jaunesnysis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "239 8242",
   "Email0": "aleksandras.konovalovas@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC369 kab.",
   "Name": "Konovalovas Aleksandras",
   "Occupation": "Dr., Mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9426",
   "Email0": "georgij.kostiuk@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV337 kab.",
   "Name": "Kostiuk Georgij",
   "Occupation": "Dr., Mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "algimantas.krisciunas@gmc.vu.lt",
   "Room": "",
   "Name": "Kriščiūnas Algimantas",
   "Occupation": "Jaunesnysis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4351",
   "Email0": "edita.kriukiene@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV315 kab.",
   "Name": "Kriukienė Edita",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "jokubas.krutkevicius@gmc.vu.lt",
   "Room": "",
   "Name": "Krutkevičius Jokūbas",
   "Occupation": "Biologas tyrėjas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4411",
   "Email0": "dalius.kuciauskas@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC221 kab.",
   "Name": "Kučiauskas Dalius",
   "Occupation": "Biologas tyrėjas, Jaunesnysis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9403",
   "Email0": "indre.kodze@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV225 kab.",
   "Name": "Kučinskaitė-Kodzė Indrė",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "egle.kupcinskaite@gmc.vu.lt",
   "Room": "",
   "Name": "Kupčinskaitė Eglė",
   "Occupation": "Laborantė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "irma.kurtinaityte@gmc.vu.lt",
   "Room": "",
   "Name": "Kurtinaitytė Irma",
   "Occupation": "Jaunesnioji administratorė, Projekto administratorė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9438",
   "Email0": "kotryna.kvederaviciute@mif.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV415 kab.",
   "Name": "Kvederavičiūtė Kotryna",
   "Occupation": "Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 9435",
   "Email0": "justas.lazutka@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC469 kab.",
   "Name": "Lazutka Justas",
   "Occupation": "Dr., Jaunesnysis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "karolis.leonavicius@gmc.vu.lt",
   "Room": "",
   "Name": "Leonavičius Karolis",
   "Occupation": "Dr., Mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9419",
   "Email0": "janina.licyte@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV313 kab.",
   "Name": "Ličytė Janina",
   "Occupation": "Jaunesnioji mokslo darbuotoja, Chemikė tyrėja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9411",
   "Email0": "darius.linge@bti.vu.lt\nwww.degama.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC317 kab.",
   "Name": "Lingė Darius",
   "Occupation": "IT technikas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "asta.luciunaite@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV229 kab.",
   "Name": "Lučiūnaitė Asta",
   "Occupation": "Biologė tyrėja, Doktorantė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9429",
   "Email0": "neringa.macijauskaite@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC449 kab.",
   "Name": "Macijauskaitė Neringa",
   "Occupation": "Biologė tyrėja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "martynas.malikenas@gmc.vu.lt",
   "Room": "",
   "Name": "Malikėnas Martynas",
   "Occupation": "Laborantas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "4353",
   "Email0": "elena.manakova@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV323 kab.",
   "Name": "Manakova Elena",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9442",
   "Email0": "mindaugas.margelevicius@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC528 kab.",
   "Name": "Margelevičius Mindaugas",
   "Occupation": "Dr., Vyresnysis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4530",
   "Email0": "viktoras.masevicius@chf.vu.lt",
   "Room": "Saulėtekio al. 3, Vilnius\nE330 kab.",
   "Name": "Masevičius Viktoras",
   "Occupation": "Dr., Vyriausiasis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4371",
   "Email0": "inga.matijosyte@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC325 kab.",
   "Name": "Matijošytė Inga",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9409",
   "Email0": "jurgita.matuliene@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC311 kab.",
   "Name": "Matulienė Jurgita",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4364",
   "Email0": "daumantas.matulis@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC309 kab.",
   "Name": "Matulis Daumantas",
   "Occupation": "Dr., Vyriausiasis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "ignas.mazelis@gmc.vu.lt",
   "Room": "",
   "Name": "Maželis Ignas",
   "Occupation": "Biologas tyrėjas, Specialistas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4356",
   "Email0": "linas.mazutis@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC225 kab.",
   "Name": "Mažutis Linas",
   "Occupation": "Dr., Vyriausiasis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9423",
   "Email0": "andrius.merkys@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV325 kab.",
   "Name": "Merkys Andrius",
   "Occupation": "Dr., Mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9411",
   "Email0": "vilma.michailoviene@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC317 kab.",
   "Name": "Michailovienė Vilma",
   "Occupation": "Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9411",
   "Email0": "aurelija.mickeviciute@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC317 kab.",
   "Name": "Mickevičiūtė Aurelija",
   "Occupation": "Biologė tyrėja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9433",
   "Email0": "gitana.mickiene@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC457 kab.",
   "Name": "Mickienė Gitana",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9418",
   "Email0": "milda.mickute@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV307 kab.",
   "Name": "Mickutė Milda",
   "Occupation": "Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9402",
   "Email0": "valdemaras.milkus@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC217 kab.",
   "Name": "Milkus Valdemaras",
   "Occupation": "Chemikas tyrėjas, Jaunesnysis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9427",
   "Email0": "irmantas.mogila@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV339 kab.",
   "Name": "Mogila Irmantas",
   "Occupation": "Jaunesnysis mokslo darbuotojas, Doktorantas, Laborantas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9400",
   "Email0": "juozas.nainys@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC219 kab.",
   "Name": "Nainys Juozas",
   "Occupation": "Jaunesnysis mokslo darbuotojas, Doktorantas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "birute.nakceriene@gmc.vu.lt",
   "Room": "",
   "Name": "Nakčerienė Birutė",
   "Occupation": "Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "milda.narmonte@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC217 kab.",
   "Name": "Narmontė Milda",
   "Occupation": "Jaunesnioji mokslo darbuotoja, Doktorantė, Chemikė tyrėja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "aurimas.nausedas@gmc.vu.lt",
   "Room": "C528 kab.",
   "Name": "Nausėdas Aurimas Aleksandras",
   "Occupation": "Chemikas tyrėjas, Doktorantas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4366",
   "Email0": "danute.noreikiene@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC525 kab.",
   "Name": "Noreikienė Danutė",
   "Occupation": "Administratorė, Projekto administratorė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9434",
   "Email0": "milda.norkiene@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC463 kab.",
   "Name": "Norkienė Milda",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "danguole.norkunaite@gmc.vu.lt",
   "Room": "",
   "Name": "Norkūnaitė Danguolė",
   "Occupation": "Specialistė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9447",
   "Email0": "kliment.olechnovic@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC534 kab.",
   "Name": "Olechnovič Kliment",
   "Occupation": "Dr., Mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4373",
   "Email0": "aleksandr.osipenko@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV307 kab.",
   "Name": "Osipenko Aleksandr",
   "Occupation": "Dr., Mokslo darbuotojas, Jaunesnysis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "vaida.paketuryte@gmc.vu.lt",
   "Room": "",
   "Name": "Paketurytė Vaida",
   "Occupation": "Vyresnioji laborantė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9433",
   "Email0": "sarunas.paskevicius@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC457 kab.",
   "Name": "Paškevičius Šarūnas",
   "Occupation": "Doktorantas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9437",
   "Email0": "inga.peciuliene@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV413 kab.",
   "Name": "Pečiulienė Inga",
   "Occupation": "Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4530",
   "Email0": "grazina.petraityte@chf.vu.lt",
   "Room": "Saulėtekio al. 3, Vilnius\nE330 kab.",
   "Name": "Petraitytė Gražina",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4421",
   "Email0": "rasa.burneikiene@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC451 kab.",
   "Name": "Petraitytė-Burneikienė Rasa",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9408",
   "Email0": "vytautas.petrauskas@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC307 kab.",
   "Name": "Petrauskas Vytautas",
   "Occupation": "Dr., Vyresnysis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9416",
   "Email0": "vilma.petrikaite@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC331 kab.",
   "Name": "Petrikaitė Vilma",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "arturas.petronis@gmc.vu.lt",
   "Room": "",
   "Name": "Petronis Artūras",
   "Occupation": "Dr., Vyriausiasis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9436",
   "Email0": "milda.pleckaityte@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC469 kab.",
   "Name": "Plečkaitytė Milda",
   "Occupation": "Dr., Vyriausioji mokslo darbuotoja, Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "karolina.pociute@gmc.vu.lt",
   "Room": "",
   "Name": "Pociutė Karolina",
   "Occupation": "Biologė tyrėja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "erikas.raginis@gmc.vu.lt",
   "Room": "",
   "Name": "Raginis Erikas",
   "Occupation": "Biologas tyrėjas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "268 7078",
   "Email0": "daiva.raguotiene@cr.vu.lt",
   "Room": "Saulėtekio al. 9 III rūmai, LT-10222 Vilnius\n625 kab.",
   "Name": "Raguotienė Daiva",
   "Occupation": "Projekto viešųjų pirkimų specialistė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "juta.rainyte@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC453 kab.",
   "Name": "Rainytė Juta",
   "Occupation": "Biologė tyrėja, Doktorantė, Specialistė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9421",
   "Email0": "rasa.rakauskaite@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV317 kab.",
   "Name": "Rakauskaitė Rasa",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9439",
   "Email0": "raimundas.razanskas@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV419 kab.",
   "Name": "Ražanskas Raimundas",
   "Occupation": "Dr., Mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9440",
   "Email0": "ausra.razanskiene@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV421 kab.",
   "Name": "Ražanskienė Aušra",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "justas.ritmejeris@gmc.vu.lt",
   "Room": "",
   "Name": "Ritmejeris Justas",
   "Occupation": "Biologas tyrėjas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "vytautas.rudokas@gmc.vu.lt",
   "Room": "",
   "Name": "Rudokas Vytautas",
   "Occupation": "Specialistas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4370",
   "Email0": "eglute.rudokiene@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV417 kab.",
   "Name": "Rudokienė Eglutė",
   "Occupation": "Vyresnioji chemikė tyrėja, Specialistė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4373",
   "Email0": "audrone.ruksenaite@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV307 kab.",
   "Name": "Rukšėnaitė Audronė",
   "Occupation": "Vyresnioji chemikė tyrėja, Chemikė tyrėja, Specialistė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "danielis.rutkauskas@gmc.vu.lt",
   "Room": "",
   "Name": "Rutkauskas Danielis",
   "Occupation": "Dr., Vyresnysis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "andrius.sakalauskas@gmc.vu.lt",
   "Room": "",
   "Name": "Sakalauskas Andrius",
   "Occupation": "Vyresnysis laborantas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "2234 9406",
   "Email0": "kestutis.sasnauskas@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC447 kab.",
   "Name": "Sasnauskas Kęstutis",
   "Occupation": "Habil. dr., Vyriausiasis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "giedrius.sasnauskas@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV331 kab.",
   "Name": "Sasnauskas Giedrius",
   "Occupation": "Dr., Vyriausiasis mokslo darbuotojas, Vyresnysis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "2234 9406",
   "Email0": "martynas.simanavicius@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV225 kab.",
   "Name": "Simanavičius Martynas",
   "Occupation": "Jaunesnysis mokslo darbuotojas, Doktorantas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "martynas.skapas@ff.vu.lt",
   "Room": "Saulėtekio al. 3, Vilnius",
   "Name": "Skapas Martynas",
   "Occupation": "Dr., Biologas tyrėjas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "kotryna.skardziute@gmc.vu.lt",
   "Room": "",
   "Name": "Skardžiūtė Kotryna",
   "Occupation": "Specialistė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "gediminas.skvarnavicius@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius",
   "Name": "Skvarnavičius Gediminas",
   "Occupation": "Jaunesnysis mokslo darbuotojas, Doktorantas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9432",
   "Email0": "rimantas.slibinskas@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC455 kab.",
   "Name": "Slibinskas Rimantas",
   "Occupation": "Dr., Vyriausiasis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "dalia.smalakyte@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV398 kab.",
   "Name": "Smalakytė Dalia",
   "Occupation": "Biologė tyrėja, Laborantė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "alexey.smirnov@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius",
   "Name": "Smirnov Alexey",
   "Occupation": "Dr., Mokslo darbuotojas, Biologas tyrėjas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "joana.smirnoviene@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC307 kab.",
   "Name": "Smirnovienė Joana",
   "Occupation": "Jaunesnioji mokslo darbuotoja, Doktorantė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9448",
   "Email0": "inga.songailiene@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV343 kab.",
   "Name": "Songailienė Inga",
   "Occupation": "Jaunesnioji mokslo darbuotoja, Specialistė, Biologė tyrėja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4369",
   "Email0": "petras.stakenas@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV411 kab.",
   "Name": "Stakėnas Petras",
   "Occupation": "Dr., Mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "vaidotas.stankevicius@gmc.vu.lt",
   "Room": "",
   "Name": "Stankevičius Vaidotas",
   "Occupation": "Dr., Mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4352",
   "Email0": "zdislav.stasevskij@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC217 kab.",
   "Name": "Staševskij Zdislav",
   "Occupation": "Jaunesnysis mokslo darbuotojas, Projekto administratorius",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "greta.stonyte@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC221 kab.",
   "Name": "Stonytė Greta",
   "Occupation": "Jaunesnioji mokslo darbuotoja, Doktorantė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9404",
   "Email0": "dovile.stravinskiene@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV227 kab.",
   "Name": "Stravinskienė Dovilė",
   "Occupation": "Jaunesnioji mokslo darbuotoja, Specialistė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "martynas.sukackas@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius",
   "Name": "Sukackas Martynas",
   "Occupation": "IT technikas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "mantas.sarauskas@gmc.vu.lt",
   "Room": "",
   "Name": "Šarauskas Mantas",
   "Occupation": "Biologas tyrėjas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9415",
   "Email0": "rimantas.siekstele@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC329 kab.",
   "Name": "Šiekštelė Rimantas",
   "Occupation": "Jaunesnysis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4354",
   "Email0": "virginijus.siksnys@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV329 kab.",
   "Name": "Šikšnys Virginijus",
   "Occupation": "Dr. (HP), Vyriausiasis mokslo darbuotojas (su išskirtinio profesoriaus kategorija), Vyresnysis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9426",
   "Email0": "arunas.silanskas@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV337 kab.",
   "Name": "Šilanskas Arūnas",
   "Occupation": "Dr., Vyresnysis mokslo darbuotojas, Mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "aurelija.sileikaite@gmc.vu.lt",
   "Room": "",
   "Name": "Šileikaitė Aurelija",
   "Occupation": "Specialistė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9428",
   "Email0": "tomas.sinkunas@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV341 kab.",
   "Name": "Šinkūnas Tomas",
   "Occupation": "Dr., Mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9407",
   "Email0": "tomas.sneideris@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC305 kab.",
   "Name": "Šneideris Tomas",
   "Occupation": "Jaunesnysis mokslo darbuotojas, Doktorantas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 9435",
   "Email0": "aliona.spakova@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC469 kab.",
   "Name": "Špakova Aliona",
   "Occupation": "Jaunesnioji mokslo darbuotoja, Doktorantė, Laborantė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 9435",
   "Email0": "paulius.tamosiunas@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV421 kab.",
   "Name": "Tamošiūnas Paulius Lukas",
   "Occupation": "Dr., Mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4357",
   "Email0": "giedre.tamulaitiene@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV335 kab.",
   "Name": "Tamulaitienė Giedrė",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9428",
   "Email0": "gintautas.tamulaitis@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV341 kab.",
   "Name": "Tamulaitis Gintautas",
   "Occupation": "Dr., Vyriausiasis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9447",
   "Email0": "kestutis.timinskas@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC534 kab.",
   "Name": "Timinskas Kęstutis",
   "Occupation": "Inžinierius tyrėjas, Jaunesnysis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9445",
   "Email0": "albertas.timinskas@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC532 kab.",
   "Name": "Timinskas Albertas",
   "Occupation": "Dr., Mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "zigmantas.toleikis@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius",
   "Name": "Toleikis Zigmantas",
   "Occupation": "Dr., Mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9448",
   "Email0": "paulius.toliusis@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV343 kab.",
   "Name": "Toliušis Paulius",
   "Occupation": "Dr., Mokslo darbuotojas, Jaunesnysis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9419",
   "Email0": "migle.tomkuviene@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV313 kab.",
   "Name": "Tomkuvienė Miglė",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "donata.tuminauskaite@gmc.vu.lt",
   "Room": "",
   "Name": "Tuminauskaitė Donata",
   "Occupation": "Specialistė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9424",
   "Email0": "ana.tunevic@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV327 kab.",
   "Name": "Tunevič Ana",
   "Occupation": "Jaunesnioji laborantė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9422",
   "Email0": "tomas.urbaitis@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV321 kab.",
   "Name": "Urbaitis Tomas",
   "Occupation": "Doktorantas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "219 3282",
   "Email0": "karolis.urbanavicius@cr.vu.lt",
   "Room": "Saulėtekio al. 9 III rūmai, LT-10222 Vilnius\n601 kab.",
   "Name": "Urbanavičius Karolis",
   "Occupation": "Projekto viešųjų pirkimų specialistas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9421",
   "Email0": "giedre.urbanaviciute@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC211 kab.",
   "Name": "Urbanavičiūtė Giedrė",
   "Occupation": "Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4395",
   "Email0": "nina.urbeliene@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC403 kab.",
   "Name": "Urbelienė Nina",
   "Occupation": "Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9423",
   "Email0": "antanas.vaitkus@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV325 kab.",
   "Name": "Vaitkus Antanas",
   "Occupation": "Jaunesnysis mokslo darbuotojas, Doktorantas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "monika.valaviciute@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC463 kab.",
   "Name": "Valavičiūtė Monika",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "laima.vasiliauskaite@mf.vu.lt",
   "Room": "",
   "Name": "Vasiliauskaitė Laima",
   "Occupation": "Biologė tyrėja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "emilija.vasiliunaite@gmc.vu.lt",
   "Room": "",
   "Name": "Vasiliūnaitė Emilija",
   "Occupation": "Laborantė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4368",
   "Email0": "ceslovas.venclovas@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC535 kab.",
   "Name": "Venclovas Česlovas",
   "Occupation": "Dr., Vyriausiasis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "tomas.venclovas@gmc.vu.lt",
   "Room": "",
   "Name": "Venclovas Tomas",
   "Occupation": "Laborantas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "arune.verbickaite@gmc.vu.lt",
   "Room": "",
   "Name": "Verbickaitė Arūnė",
   "Occupation": "Vyresnioji laborantė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "gene.vercholamova@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC003 kab.",
   "Name": "Vercholamova Genė",
   "Occupation": "Autoklavinės operatorė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9414",
   "Email0": "ausra.veteikyte@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC323 kab.",
   "Name": "Veteikytė Aušra",
   "Occupation": "Biologė tyrėja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9437",
   "Email0": "laurynas.vilys@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV413 kab.",
   "Name": "Vilys Laurynas",
   "Occupation": "Jaunesnysis mokslo darbuotojas, Biologas tyrėjas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4372",
   "Email0": "giedrius.vilkaitis@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV305 kab.",
   "Name": "Vilkaitis Giedrius",
   "Occupation": "Dr., Vyriausiasis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9448",
   "Email0": "evelina.zagorskaite@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV343 kab.",
   "Name": "Zagorskaitė Evelina",
   "Occupation": "Dr., Jaunesnioji mokslo darbuotoja, Specialistė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9413",
   "Email0": "audrius.zaksauskas@bti.vu.lt\nhttp://www.bti.vu.lt/",
   "Room": "Saulėtekio al. 7, Vilnius\nC321 kab.",
   "Name": "Zakšauskas Audrius",
   "Occupation": "Jaunesnysis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4357",
   "Email0": "mindaugas.zaremba@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV335 kab.",
   "Name": "Zaremba Mindaugas",
   "Occupation": "Dr., Vyriausiasis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9431",
   "Email0": "mindaugas.zaveckas@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC453 kab.",
   "Name": "Zaveckas Mindaugas",
   "Occupation": "Dr., Mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9429",
   "Email0": "ruta.zinkeviciute@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC449 kab.",
   "Name": "Zinkevičiūtė Rūta",
   "Occupation": "Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9412",
   "Email0": "asta.zubriene@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC319 kab.",
   "Name": "Zubrienė Asta",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "rimante.zedaveinyte@gmc.vu.lt",
   "Room": "",
   "Name": "Žedaveinytė Rimantė",
   "Occupation": "Biologė tyrėja, Specialistė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "ausra.zelvyte@gmc.vu.lt",
   "Room": "",
   "Name": "Želvytė Aušra",
   "Occupation": "Vyresnioji laborantė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "",
   "Email0": "mantas.ziaunys@gmc.vu.lt",
   "Room": "",
   "Name": "Žiaunys Mantas",
   "Occupation": "Jaunesnysis mokslo darbuotojas, Doktorantas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9400",
   "Email0": "rapolas.zilionis@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC219 kab.",
   "Name": "Žilionis Rapolas",
   "Occupation": "Jaunesnysis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9434",
   "Email0": "danguole.ziogiene@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC463 kab.",
   "Name": "Žiogienė Danguolė",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "9432",
   "Email0": "eimantas.zitkus@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC455 kab.",
   "Name": "Žitkus Eimantas",
   "Occupation": "Jaunesnysis mokslo darbuotojas, Doktorantas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4365 9463",
   "Email0": "janina.ziukaite@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC526 kab.",
   "Name": "Žiūkaitė Janina",
   "Occupation": "Specialistė personalui, Projekto administratorė",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4360",
   "Email0": "aurelija.zvirbliene@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV231 kab.",
   "Name": "Žvirblienė Aurelija",
   "Occupation": "Dr. (HP), Vyriausioji mokslo darbuotoja",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4358",
   "Email0": "gintautas.zvirblis@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC461 kab.",
   "Name": "Žvirblis Gintautas",
   "Occupation": "Dr., Vyresnysis mokslo darbuotojas",
   "Institute": "Institute of Biotechnology"
 },
 {
   "Phone": "223 4378",
   "Email0": "kastis.krikstopaitis@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC508 kab.",
   "Name": "Krikštopaitis Kastis",
   "Occupation": "Dr., Direktorius, Vyresnysis mokslo darbuotojas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4424",
   "Email0": "rokas.abraitis@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, LT-10257 Vilnius\nC151 kab.",
   "Name": "Abraitis Rokas",
   "Occupation": "Dr., Projekto administratorius",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4414",
   "Email0": "tatjana.akopova@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV221 kab.",
   "Name": "Akopova Tatjana",
   "Occupation": "Laborantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "indre.aleknaviciene@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius",
   "Name": "Aleknavičienė Indrė",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4413",
   "Email0": "milda.peciukaityte@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV217 kab.",
   "Name": "Alksnė Milda",
   "Occupation": "Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "filipas.ambrulevicius@chf.vu.lt",
   "Room": "Naugarduko g. 24, Vilnius\n204, 241 kab.",
   "Name": "Ambrulevičius Filipas",
   "Occupation": "Doktorantas, Specialistas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4414",
   "Email0": "daiva.andriuniene@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC223 kab.",
   "Name": "Andriūnienė Daiva",
   "Occupation": "Laborantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4405",
   "Email0": "zilvinas.anusevicius@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC425 kab.",
   "Name": "Anusevičius Žilvinas",
   "Occupation": "Dr., Vyresnysis mokslo darbuotojas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4383",
   "Email0": "agota.aucynaite@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV401 kab.",
   "Name": "Aučynaitė Agota",
   "Occupation": "Dr., Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4400",
   "Email0": "nijole.baliuckiene@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC415 kab.",
   "Name": "Baliuckienė Nijolė",
   "Occupation": "Vyresnioji laborantė, Projekto specialistė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4379",
   "Email0": "daiva.baltriukiene@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC509 kab.",
   "Name": "Baltriukienė Daiva",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "4417",
   "Email0": "emilija.baltrukonyte@gmc.vu.lt",
   "Room": "V-011 kab.",
   "Name": "Baltrukonytė Emilija",
   "Occupation": "Specialistė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "raminta.bausyte@gmc.vu.lt",
   "Room": "",
   "Name": "Baušytė Raminta",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4381",
   "Email0": "alina.bedulskaja@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC511 kab.",
   "Name": "Bedulskaja Alina",
   "Occupation": "Jaunesnioji administratorė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "svetlana.belik@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC457 kab.",
   "Name": "Belik Svetlana",
   "Occupation": "Laborantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4406",
   "Email0": "veronika.borutinskaite@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV201 kab.",
   "Name": "Borutinskaitė Veronika Viktorija",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "85272 9068",
   "Email0": "jekaterina.borzova@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius",
   "Name": "Borzova Jekaterina",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 7391",
   "Email0": "irina.bratkovskaja@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC437 kab.",
   "Name": "Bratkovskaja Irina",
   "Occupation": "Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "85272 9186",
   "Email0": "rima.budvytyte@gmc.vu.lt",
   "Room": "Mokslininkų g. 12, Vilnius\n108 kab.",
   "Name": "Budvytytė Rima",
   "Occupation": "Dr., Mokslo darbuotoja, Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4408",
   "Email0": "virginija.bukelskiene@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV205 kab.",
   "Name": "Bukelskienė Virginija",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4402",
   "Email0": "arunas.bulovas@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC419 kab.",
   "Name": "Bulovas Arūnas",
   "Occupation": "Dr., Biochemikas tyrėjas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4382",
   "Email0": "aurelijus.burokas@gmc.vu.lt",
   "Room": "C503 kab.",
   "Name": "Burokas Aurelijus",
   "Occupation": "Dr., Vyresnysis mokslo darbuotojas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4387",
   "Email0": "marius.butkevicius@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC429 kab.",
   "Name": "Butkevičius Marius",
   "Occupation": "Doktorantas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "jonas.cicenas@gmc.vu.lt\nhttp://www.mapkinases.eu/founder/",
   "Room": "Sauletekio 7\n506 kab.",
   "Name": "Cicėnas Jonas",
   "Occupation": "Dr., Vyresnysis mokslo darbuotojas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4397",
   "Email0": "vida.casaite@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC407 kab.",
   "Name": "Časaitė Vida",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4425",
   "Email0": "laimute.cekutiene@cr.vu.lt",
   "Room": "Saulėtekio al.7, Vilnius\nC513 kab.",
   "Name": "Čekutienė Laimutė",
   "Occupation": "Projekto viešųjų pirkimų specialistė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4392",
   "Email0": "narimantas.cenas@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC439 kab.",
   "Name": "Čėnas Narimantas",
   "Occupation": "Habil. dr., Vyriausiasis mokslo darbuotojas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4389",
   "Email0": "marius.dagys@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC433 kab.",
   "Name": "Dagys Marius",
   "Occupation": "Dr., Mokslo darbuotojas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4415",
   "Email0": "nadezda.dreize@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV223 kab.",
   "Name": "Dreižė Nadežda",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "239 8217",
   "Email0": "redas.dulinskas@gf.vu.lt",
   "Room": "Saulėtekio al.7, Vilnius\nV123 kab.",
   "Name": "Dulinskas Redas",
   "Occupation": "Jaunesnysis mokslo darbuotojas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4399",
   "Email0": "virginija.dzekeviciene@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC411, V412 kab.",
   "Name": "Dzekevičienė Virginija",
   "Occupation": "Vyresnioji laborantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4412",
   "Email0": "monika.gasiuniene@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV215 kab.",
   "Name": "Gasiūnienė Monika",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4396",
   "Email0": "renata.gasparaviciute@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC405 kab.",
   "Name": "Gasparavičiūtė Renata",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4411",
   "Email0": "marija.ger@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV213 kab.",
   "Name": "Ger Marija",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4400",
   "Email0": "laima.ginotiene@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC415 kab.",
   "Name": "Ginotienė Laima",
   "Occupation": "Laborantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4390",
   "Email0": "justina.gruzauskaite@gmc.vu.lt",
   "Room": "C4335 kab.",
   "Name": "Gružauskaitė Justina",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4388",
   "Email0": "vidute.gureviciene@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC431 kab.",
   "Name": "Gurevičienė Vidutė",
   "Occupation": "Biochemikė tyrėja, Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4416",
   "Email0": "ausra.imbrasaite@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC203 kab.",
   "Name": "Imbrasaitė Aušra",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4383",
   "Email0": "jevgenija.jakubovska@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV401 kab.",
   "Name": "Jakubovska Jevgenija",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "85272 9058",
   "Email0": "regina.janciene@bchi.vu.lt",
   "Room": "Mokslininkų 12a, Vilnius\n239 kab.",
   "Name": "Jančienė Regina",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4400",
   "Email0": "marija.jankunec@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC415 kab.",
   "Name": "Jankunec Marija",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "rasa.jarasiene@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius",
   "Name": "Jarašienė-Burinskaja Rasa",
   "Occupation": "Dr., Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "239 8229",
   "Email0": "violeta.jonusiene@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV241 kab.",
   "Name": "Jonušienė Violeta",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4400",
   "Email0": "algimantas.jonuska@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC415 kab.",
   "Name": "Jonuška Algimantas",
   "Occupation": "Įrangos techninės priežiūros inžinierius, Projekto specialistas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4380",
   "Email0": "eugenijus.jovaisa@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC510 kab.",
   "Name": "Jovaiša Eugenijus",
   "Occupation": "Administratorius, Projekto administratorius",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "benediktas.juodka@cr.vu.lt",
   "Room": "",
   "Name": "Juodka Benediktas",
   "Occupation": "Habil. dr., Profesorius emeritas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4399",
   "Email0": "lina.juskiene@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC411 kab.",
   "Name": "Juškienė Lina",
   "Occupation": "Vyresnioji laborantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4384",
   "Email0": "laura.kaliniene@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV403 kab.",
   "Name": "Kalinienė Laura",
   "Occupation": "Dr., Mokslo darbuotoja, Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4407",
   "Email0": "audrone.kalvelyte@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV203 kab.",
   "Name": "Kalvelytė Audronė Valerija",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "giedre.karzaite@gmc.vu.lt",
   "Room": "",
   "Name": "Karzaitė Giedrė",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4411",
   "Email0": "algirdas.kaupinis@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV213 kab.",
   "Name": "Kaupinis Algirdas",
   "Occupation": "Dr., Mokslo darbuotojas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4381",
   "Email0": "rima.kemesiene@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC511 kab.",
   "Name": "Kemėšienė Rima",
   "Occupation": "Aptarnaujanti specialistė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "jurate.kernagyte@gmc.vu.lt",
   "Room": "",
   "Name": "Kernagytė Jūratė",
   "Occupation": "Laborantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "85272 9058",
   "Email0": "algirdas.klimavicius@bchi.vu.lt",
   "Room": "Mokslininkų 12a, Vilnius\n224 kab.",
   "Name": "Klimavičius Kazimieras Algirdas",
   "Occupation": "Dr., Vyresnysis biochemikas tyrėjas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4393",
   "Email0": "lidija.kosychova@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC441 kab.",
   "Name": "Kosychova Lidija",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4407",
   "Email0": "natalija.krestnikova@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV203 kab.",
   "Name": "Krestnikova Natalija",
   "Occupation": "Dr., Jaunesnioji mokslo darbuotoja, Mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "arunas.krikstaponis@bchi.vu.lt",
   "Room": "",
   "Name": "Krikštaponis Arūnas",
   "Occupation": "Doktorantas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "algimantas.krutkis@bchi.vu.lt",
   "Room": "Mokslininkų g. 12a, Vilnius\n221 kab.",
   "Name": "Krutkis Algimantas",
   "Occupation": "Įrangos techninės priežiūros inžinierius",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4400 223 4419",
   "Email0": "danute.kureckiene@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC415 kab.",
   "Name": "Kurečkienė Danutė",
   "Occupation": "Laborantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4387",
   "Email0": "audrius.laurynenas@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC429 kab.",
   "Name": "Laurynėnas Audrius",
   "Occupation": "Jaunesnysis mokslo darbuotojas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "mindaugas.lesanavicius@gmc.vu.lt",
   "Room": "",
   "Name": "Lesanavičius Mindaugas",
   "Occupation": "Doktorantas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "85272 9058",
   "Email0": "ilona.levutiene@bchi.vu.lt",
   "Room": "Mokslininkų 12a, Vilnius\n223 kab.",
   "Name": "Levutienė Ilona",
   "Occupation": "Laborantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "jonas.liokaitis@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius",
   "Name": "Liokaitis Jonas",
   "Occupation": "IT technikas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "9429",
   "Email0": "neringa.macijauskaite@bti.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC449 kab.",
   "Name": "Macijauskaitė Neringa",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "kristina.majauskaite@bchi.vu.lt",
   "Room": "M.K.Čiurlionio g. 21-27\n234 kab.",
   "Name": "Majauskaitė Kristina",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4403",
   "Email0": "audrone.maroziene@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC421 kab.",
   "Name": "Marozienė Audronė",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4393",
   "Email0": "jadvyga.ilinykh@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC441 kab.",
   "Name": "Matulevič Ilinykh Jadvyga",
   "Occupation": "Vyresnioji laborantė, Projekto specialistė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "85272 9058",
   "Email0": "jonas.meskauskas@bchi.vu.lt",
   "Room": "Mokslininkų 12a, Vilnius\n238 kab.",
   "Name": "Meškauskas Jonas",
   "Occupation": "Biochemikas tyrėjas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "219 5022",
   "Email0": "tadas.meskauskas@mif.vu.lt",
   "Room": "Didlaukio g. 47/Naugarduko g.24, Vilnius\n402/118 kab.",
   "Name": "Meškauskas Tadas",
   "Occupation": "Dr., Vyriausiasis mokslo darbuotojas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4398",
   "Email0": "rita.meskiene@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC409 kab.",
   "Name": "Meškienė Rita",
   "Occupation": "Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "ingrida.meskinyte@gmc.vu.lt",
   "Room": "",
   "Name": "Meškinytė Ingrida",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4386",
   "Email0": "rolandas.meskys@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV407 kab.",
   "Name": "Meškys Rolandas",
   "Occupation": "Dr., Vyriausiasis mokslo darbuotojas (su išskirtinio profesoriaus kategorija)",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "marija.meskyte@gmc.vu.lt",
   "Room": "",
   "Name": "Meškytė Erna Marija",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4403",
   "Email0": "lina.miseviciene@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC421 kab.",
   "Name": "Misevičienė Lina",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4409",
   "Email0": "ruta.navakauskiene@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV207 kab.",
   "Name": "Navakauskienė Rūta",
   "Occupation": "Dr. (HP), Vyriausioji mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "ausra.nemeikaite-ceniene@gmc.vu.lt",
   "Room": "",
   "Name": "Nemeikaitė-Čėnienė Aušra",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4402",
   "Email0": "gediminas.niaura@ff.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC419 kab.",
   "Name": "Niaura Gediminas",
   "Occupation": "Dr., Vyriausiasis mokslo darbuotojas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "algirdas.noreika@gmc.vu.lt",
   "Room": "",
   "Name": "Noreika Algirdas",
   "Occupation": "Doktorantas, Biologas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "vilija.novikiene@gmc.vu.lt",
   "Room": "",
   "Name": "Novikienė Vilija",
   "Occupation": "Laborantė, Projekto specialistė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "85272 9058",
   "Email0": "stanislava.palaikiene@bchi.vu.lt",
   "Room": "Mokslininkų 12a, Vilnius\n131 kab.",
   "Name": "Palaikienė Stanislava",
   "Occupation": "Jaunesnioji administratorė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4401",
   "Email0": "bozena.pavliukeviciene@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC417 kab.",
   "Name": "Pavliukevičienė Božena",
   "Occupation": "Dr., Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4390",
   "Email0": "tadas.penkauskas@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC435 kab.",
   "Name": "Penkauskas Tadas",
   "Occupation": "Doktorantas, Projekto specialistas, Specialistas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "85272 9058",
   "Email0": "ala.peteraitiene@bchi.vu.lt",
   "Room": "Mokslininkų 12a, Vilnius\n239 kab.",
   "Name": "Peteraitienė Ala",
   "Occupation": "Laborantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4383",
   "Email0": "vytautas.petkevicius@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV401 kab.",
   "Name": "Petkevičius Vytautas",
   "Occupation": "Jaunesnysis mokslo darbuotojas, Specialistas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "85272 9058",
   "Email0": "daiva.podeniene@bchi.vu.lt",
   "Room": "Mokslininkų 12a, Vilnius\n224 kab.",
   "Name": "Podėnienė Daiva",
   "Occupation": "Jaunesnioji administratorė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4404",
   "Email0": "evelina.polmickaite-smirnova@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC423 kab.",
   "Name": "Polmickaitė-Smirnova Evelina",
   "Occupation": "Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4397",
   "Email0": "simona.poviloniene@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC407 kab.",
   "Name": "Povilonienė Simona",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4402",
   "Email0": "giulio.preta@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC419 kab.",
   "Name": "Preta Giulio",
   "Occupation": "Dr., Vyresnysis mokslo darbuotojas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "272 9069",
   "Email0": "juozas.puzas@gmc.vu.lt",
   "Room": "Mokslininkų g. 12A, Vilnius\n113 kab.",
   "Name": "Puzas Juozas",
   "Occupation": "Projekto specialistas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4390",
   "Email0": "ingrida.jurkeviciute@gmc.vu.lt",
   "Room": "C435 kab.",
   "Name": "Radveikienė Ingrida",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4401",
   "Email0": "tadas.ragaliauskas@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC417 kab.",
   "Name": "Ragaliauskas Tadas",
   "Occupation": "Dr., Mokslo darbuotojas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "tomas.raila@mif.vu.lt",
   "Room": "",
   "Name": "Raila Tomas",
   "Occupation": "Specialistas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "eimantas.ramonas@gmc.vu.lt",
   "Room": "",
   "Name": "Ramonas Eimantas",
   "Occupation": "Doktorantas, Projekto specialistas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4390",
   "Email0": "dalius.ratautas@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC435 kab.",
   "Name": "Ratautas Dalius",
   "Occupation": "Dr., Jaunesnysis mokslo darbuotojas, Projekto specialistas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4388",
   "Email0": "julija.razumiene@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC431 kab.",
   "Name": "Razumienė Julija",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja, Vyriausioji mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "2239 8253",
   "Email0": "radvile.rimgaile-voicik@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV145 kab.",
   "Name": "Rimgailė-Voicik Radvilė",
   "Occupation": "Dr., Projekto administratorė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "(8 5) 223 4417",
   "Email0": "ieva.rinkunaite@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV011 kab.",
   "Name": "Rinkūnaitė Ieva",
   "Occupation": "Doktorantė, Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "85272 9058",
   "Email0": "ricardas.rozenbergas@bchi.vu.lt",
   "Room": "Mokslininkų 12a, Vilnius\n224 kab.",
   "Name": "Rozenbergas Ričardas",
   "Occupation": "Biochemikas tyrėjas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4398",
   "Email0": "rasa.rutkiene@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC409 kab.",
   "Name": "Rutkienė Rasa",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4395",
   "Email0": "mikas.sadauskas@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC403 kab.",
   "Name": "Sadauskas Mikas",
   "Occupation": "Doktorantas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "matas.sereika@gmc.vu.lt",
   "Room": "",
   "Name": "Sereika Matas",
   "Occupation": "Projekto specialistas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "85272 9058",
   "Email0": "romualdas.sirutkaitis@gmc.vu.lt",
   "Room": "Mokslininkų 12a, Vilnius\n225 kab.",
   "Name": "Sirutkaitis Romualdas Aleksas",
   "Occupation": "Dr., Biochemikas tyrėjas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "85272 9058",
   "Email0": "olga.sivova@bchi.vu.lt",
   "Room": "Mokslininkų 12a, Vilnius\n239 kab.",
   "Name": "Sivova Olga",
   "Occupation": "Laborantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4398",
   "Email0": "ruta.stanislauskiene@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC409 kab.",
   "Name": "Stanislauskienė Rūta",
   "Occupation": "Dr., Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "85272 9058",
   "Email0": "zita.staniulyte@bchi.vu.lt",
   "Room": "Mokslininkų 12a, Vilnius\n223 kab.",
   "Name": "Staniulytė Zita",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4397",
   "Email0": "jonita.stankeviciute@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC407 kab.",
   "Name": "Stankevičiūtė Jonita",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "(8 5) 223 4407",
   "Email0": "aurimas.stulpinas@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV203 kab.",
   "Name": "Stulpinas Aurimas",
   "Occupation": "Jaunesnysis mokslo darbuotojas, Doktorantas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "asta.stulpiniene@gmc.vu.lt",
   "Room": "",
   "Name": "Stulpinienė Asta",
   "Occupation": "Projekto administratorė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4388",
   "Email0": "ieva.sakinyte@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC431 kab.",
   "Name": "Šakinytė Ieva",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4404",
   "Email0": "jonas.sarlauskas@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC423 kab.",
   "Name": "Šarlauskas Jonas",
   "Occupation": "Dr., Vyresnysis mokslo darbuotojas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4417",
   "Email0": "alvile.scerbaviciene@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV011 kab.",
   "Name": "Ščerbavičienė Alvilė",
   "Occupation": "Veterinarijos gydytoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4387",
   "Email0": "remigijus.simkus@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC429 kab.",
   "Name": "Šimkus Remigijus",
   "Occupation": "Dr., Vyresnysis mokslo darbuotojas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "egidijus.simoliunas@gmc.vu.lt",
   "Room": "",
   "Name": "Šimoliūnas Egidijus",
   "Occupation": "Doktorantas, Jaunesnysis mokslo darbuotojas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4395",
   "Email0": "eugenijus.simoliunas@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC403 kab.",
   "Name": "Šimoliūnas Eugenijus",
   "Occupation": "Dr., Mokslo darbuotojas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "monika.simoliuniene@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius",
   "Name": "Šimoliūnienė Monika",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4400",
   "Email0": "martynas.talaikis@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC415 kab.",
   "Name": "Talaikis Martynas",
   "Occupation": "Biochemikas tyrėjas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4384",
   "Email0": "daiva.tauraite@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV403 kab.",
   "Name": "Tauraitė Daiva",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 7391",
   "Email0": "lidija.tetianec@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC437 kab.",
   "Name": "Tetianec Lidija",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4406",
   "Email0": "grazina.treigyte@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV201 kab.",
   "Name": "Treigytė Gražina",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4385",
   "Email0": "lidija.truncaite@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV405 kab.",
   "Name": "Truncaitė Lidija",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "saulius.tumenas@gmc.vu.lt",
   "Room": "",
   "Name": "Tumėnas Saulius",
   "Occupation": "Dr., Vyresnysis mokslo darbuotojas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "ausra.unguryte@gmc.vu.lt",
   "Room": "",
   "Name": "Ungurytė Aušra",
   "Occupation": "Dr., Projekto mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4418",
   "Email0": "vida.untaniene@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV013 kab.",
   "Name": "Untanienė Vida Stefa",
   "Occupation": "Laborantė gyvūnų priežiūrai",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4395",
   "Email0": "nina.urbeliene@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC403 kab.",
   "Name": "Urbelienė Nina",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "gintare.urbonaite@gmc.vu.lt",
   "Room": "",
   "Name": "Urbonaitė Gintarė",
   "Occupation": "Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "(8 5) 239 4231",
   "Email0": "rytis.urbonas@chf.vu.lt",
   "Room": "V.A. Graičiūno g. 8, Vilnius",
   "Name": "Urbonas Rytis Vincentas",
   "Occupation": "Jaunesnysis administratorius",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4384",
   "Email0": "jaunius.urbonavicius@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV403 kab.",
   "Name": "Urbonavičius Jaunius",
   "Occupation": "Dr., Vyresnysis mokslo darbuotojas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "silvija.urnikyte@gmc.vu.lt",
   "Room": "",
   "Name": "Urnikytė Silvija",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4399",
   "Email0": "nijole.uzdaviniene@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC411 kab.",
   "Name": "Uždavinienė Nijolė",
   "Occupation": "Vyresnioji laborantė, Laborantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4396",
   "Email0": "justas.vaitekunas@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC405 kab.",
   "Name": "Vaitekūnas Justas",
   "Occupation": "Jaunesnysis mokslo darbuotojas, Specialistas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4393",
   "Email0": "benjaminas.valiauga@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC441 kab.",
   "Name": "Valiauga Benjaminas",
   "Occupation": "Doktorantas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4435",
   "Email0": "gintaras.valincius@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC155, C445 kab.",
   "Name": "Valinčius Gintaras",
   "Occupation": "Dr., Vyriausiasis mokslo darbuotojas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "giedre.valiuliene@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius",
   "Name": "Valiulienė Giedrė",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4410",
   "Email0": "mindaugas.valius@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV211 kab.",
   "Name": "Valius Mindaugas",
   "Occupation": "Dr., Vyresnysis mokslo darbuotojas",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4418",
   "Email0": "liudvika.vaskeviciene@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV013 kab.",
   "Name": "Vaškevičienė Liudvika",
   "Occupation": "Laborantė gyvūnų priežiūrai",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "vilte.vetlovaite@gmc.vu.lt",
   "Room": "",
   "Name": "Vetlovaitė Viltė",
   "Occupation": "Projekto specialistė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 7391",
   "Email0": "regina.vidziunaite@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC437 kab.",
   "Name": "Vidžiūnaitė Regina",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4412",
   "Email0": "aida.vitkeviciene@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV215 kab.",
   "Name": "Vitkevičienė Aida",
   "Occupation": "Doktorantė, Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "223 4385",
   "Email0": "aurelija.zajanckauskaite@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV405 kab.",
   "Name": "Zajančkauskaitė Aurelija",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "",
   "Email0": "aiste.zentelyte@bchi.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC203 kab.",
   "Name": "Zentelytė Aistė",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "37061570919",
   "Email0": "egle.zalyte@gf.vu.lt",
   "Room": "Saulėtekio al. 7\nV237 kab.",
   "Name": "Žalytė Eglė",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Biochemistry"
 },
 {
   "Phone": "239 8257",
   "Email0": "juozas.lazutka@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC229 kab.",
   "Name": "Lazutka Juozas Rimantas",
   "Occupation": "Habil. dr., Direktorius, Profesorius",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8210",
   "Email0": "egle.lastauskiene@gf.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nC349 kab.",
   "Name": "Lastauskienė Eglė",
   "Occupation": "Dr., Direktoriaus pavaduotoja, Docentė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8240",
   "Email0": "janina.adomaitiene@gf.vu.lt",
   "Room": "Saulėtekio al.7, Vilnius\nV138 kab.",
   "Name": "Adomaitienė Janina",
   "Occupation": "Jaunesnioji administratorė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "mantas.adomaitis@gmc.vu.lt",
   "Room": "C135 kab.",
   "Name": "Adomaitis Mantas",
   "Occupation": "Jaunesnysis laborantas, Doktorantas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8242",
   "Email0": "lina.aitmanaite@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC369 kab.",
   "Name": "Aitmanaitė Lina",
   "Occupation": "Jaunesnioji asistentė, Jaunesnioji mokslo darbuotoja, Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "223 4438",
   "Email0": "aidas.alaburda@gf.vu.lt",
   "Room": "Saulėtekio al.7, Vilnius\nV135 kab.",
   "Name": "Alaburda Aidas",
   "Occupation": "Dr., Profesorius",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8230",
   "Email0": "julija.armalyte@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC355 kab.",
   "Name": "Armalytė Julija",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja, Docentė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "tatjana.iznova@gf.vu.lt",
   "Room": "",
   "Name": "Arslanova Tatjana",
   "Occupation": "Dr., Vyresnioji specialistė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8265",
   "Email0": "rasa.butautaite@gf.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nC119 kab.",
   "Name": "Aukštikalnienė Rasa",
   "Occupation": "Dr., Lektorė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "223 4449 239 8231",
   "Email0": "jolanta.bagvile@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC143, C363 kab.",
   "Name": "Bagvilė Jolanta",
   "Occupation": "Jaunesnioji administratorė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "laura.baliulyte@gmc.vu.lt",
   "Room": "",
   "Name": "Baliulytė Laura",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "238 8241",
   "Email0": "vida.bendikiene@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC351 kab.",
   "Name": "Bendikienė Vida",
   "Occupation": "Dr. (HP), Afilijuotoji mokslininkė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8265",
   "Email0": "rasa.bernotiene@gmc.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nC119 kab.",
   "Name": "Bernotienė Rasa",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "ruslan.bikmurzin@gmc.vu.lt",
   "Room": "",
   "Name": "Bikmurzin Ruslan",
   "Occupation": "Doktorantas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8267",
   "Email0": "marija.biteniekyte@gf.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nC121 kab.",
   "Name": "Biteniekytė Marija",
   "Occupation": "Dr., Lektorė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8263",
   "Email0": "rima.briskaite@gf.vu.lt",
   "Room": "Saulėtekio al.7, Vilnius\nV147 kab.",
   "Name": "Briškaitė Rima",
   "Occupation": "Jaunesnioji administratorė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8217",
   "Email0": "rokas.buisas@gf.vu.lt",
   "Room": "Saulėtekio al.7, Vilnius\nV123 kab.",
   "Name": "Buišas Rokas",
   "Occupation": "Dr., Asistentas, Jaunesnysis mokslo darbuotojas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8266",
   "Email0": "egidijus.bukelskis@gf.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nC117 kab.",
   "Name": "Bukelskis Egidijus",
   "Occupation": "Dr., Docentas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8214",
   "Email0": "inga.burneikiene@gf.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nC339 kab.",
   "Name": "Burneikienė Inga",
   "Occupation": "Jaunesnioji asistentė, Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "eugenijus.butkus@chf.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nC155 kab.",
   "Name": "Butkus Eugenijus",
   "Occupation": "Habil. dr., Profesorius",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "223 4443",
   "Email0": "jurgita.butkuviene@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius",
   "Name": "Butkuvienė Jurgita",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8261",
   "Email0": "egle.cesniene@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV429 kab.",
   "Name": "Čėsnienė Tatjana",
   "Occupation": "Dr., Docentė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "sigita.cinciute@gmc.vu.lt",
   "Room": "v139 kab.",
   "Name": "Činčiūtė Sigita",
   "Occupation": "Jaunesnioji laborantė, Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8224",
   "Email0": "daiva.dabkeviciene@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV239 kab.",
   "Name": "Dabkevičienė Daiva",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8219",
   "Email0": "algis.daktariunas@gf.vu.lt",
   "Room": "Saulėtekio al.7, Vilnius\nV139 kab.",
   "Name": "Daktariūnas Algis",
   "Occupation": "Jaunesnysis mokslo darbuotojas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "jurij.danilov@gmc.vu.lt",
   "Room": "",
   "Name": "Danilov Jurij",
   "Occupation": "Doktorantas, Jaunesnysis mokslo darbuotojas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "julija.daniloviene@gmc.vu.lt",
   "Room": "",
   "Name": "Danilovienė Julija",
   "Occupation": "Jungtinė doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "223 4441",
   "Email0": "kristina.daniunaite@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC205 kab.",
   "Name": "Daniūnaitė Kristina",
   "Occupation": "Dr., Mokslo darbuotoja, Docentė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "denisas.dankinas@gf.vu.lt",
   "Room": "",
   "Name": "Dankinas Denisas",
   "Occupation": "Doktorantas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "dominyka.dapkute@gmc.vu.lt",
   "Room": "",
   "Name": "Dapkutė Dominyka",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8229",
   "Email0": "neringa.vezelyte@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV241 kab.",
   "Name": "Daugelavičienė Neringa",
   "Occupation": "Jaunesnioji mokslo darbuotoja, Jaunesnioji asistentė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8260",
   "Email0": "veronika.dedonyte@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC231 kab.",
   "Name": "Dedonytė Veronika",
   "Occupation": "Dr., Mokslo darbuotoja, Docentė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "(8 5) 278 6709",
   "Email0": "monika.drobniene@nvi.lt",
   "Room": "",
   "Name": "Drobnienė Monika",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8217",
   "Email0": "redas.dulinskas@gf.vu.lt",
   "Room": "Saulėtekio al.7, Vilnius\nV123 kab.",
   "Name": "Dulinskas Redas",
   "Occupation": "Jaunesnysis laborantas, Doktorantas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8209",
   "Email0": "audrius.gegeckas@gf.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nC341 kab.",
   "Name": "Gegeckas Audrius",
   "Occupation": "Dr., Asistentas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "vilmantas.gegzna@tmi.vu.lt\nhttp://web.vu.lt/gmc/v.gegzna/",
   "Room": "Saulėtekio al. 3, Vilnius\nE442, NFTMC kab.",
   "Name": "Gėgžna Vilmantas",
   "Occupation": "Lektorius",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "ina.gorban@gmc.vu.lt",
   "Room": "",
   "Name": "Gorban Ina",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "aida.grabauskaite@gf.vu.lt",
   "Room": "",
   "Name": "Grabauskaitė Aida",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8214",
   "Email0": "alisa.gricajeva@gf.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nC339 kab.",
   "Name": "Gricajeva Alisa",
   "Occupation": "Jaunesnioji asistentė, Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8218",
   "Email0": "ramune.griksiene@gf.vu.lt",
   "Room": "Saulėtekio al.7, Vilnius\nV129 kab.",
   "Name": "Grikšienė Ramunė",
   "Occupation": "Dr., Docentė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "223 4439",
   "Email0": "inga.griskova-bulanova@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV137 kab.",
   "Name": "Griškova-Bulanova Inga",
   "Occupation": "Dr., Vyriausioji mokslo darbuotoja",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8248",
   "Email0": "nijole.gudaviciene@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV423 kab.",
   "Name": "Gudavičienė Nijolė",
   "Occupation": "Laborantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8209",
   "Email0": "renata.gudiukaite@gf.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nC341 kab.",
   "Name": "Gudiukaitė Renata",
   "Occupation": "Dr., Asistentė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8270",
   "Email0": "jekaterina.havelka@gf.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nC133 kab.",
   "Name": "Havelka Jekaterina",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "223 4433",
   "Email0": "gytautas.ignatavicius@gf.vu.lt",
   "Room": "Saulėtekio al.7, Vilnius\nV105 kab.",
   "Name": "Ignatavičius Gytautas",
   "Occupation": "Dr., Docentas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "mikas.ilgunas@gmc.vu.lt",
   "Room": "",
   "Name": "Ilgūnas Mikas",
   "Occupation": "Jungtinis doktorantas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "276 8242",
   "Email0": "egle.janenaite@gmc.vu.lt",
   "Room": "",
   "Name": "Janėnaitė Eglė",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "219 3242",
   "Email0": "sonata.jarmalaite@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC209 kab.",
   "Name": "Jarmalaitė Sonata",
   "Occupation": "Dr. (HP), Profesorė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "greta.jarockyte@gmc.vu.lt",
   "Room": "",
   "Name": "Jarockytė Greta",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8269",
   "Email0": "nijole.jasenoviciene@gf.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nC131 kab.",
   "Name": "Jasenovičienė Nijolė",
   "Occupation": "Jaunesnioji administratorė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8229",
   "Email0": "violeta.jonusiene@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV241 kab.",
   "Name": "Jonušienė Violeta",
   "Occupation": "Dr., Mokslo darbuotoja, Docentė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "justina.jurgeleviciute@gmc.vu.lt",
   "Room": "",
   "Name": "Jurgelevičiūtė Justina",
   "Occupation": "Jaunesnioji asistentė, Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8206",
   "Email0": "ramune.jurkeviciene@gf.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nC347 kab.",
   "Name": "Jurkevičienė Ramunė",
   "Occupation": "Jaunesnioji administratorė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "223 4440",
   "Email0": "sigitas.juzenas@gf.vu.lt",
   "Room": "Saulėtekio al.7, Vilnius\nV143 kab.",
   "Name": "Juzėnas Sigitas",
   "Occupation": "Lektorius",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8750",
   "Email0": "virginija.kalciene@gf.vu.lt",
   "Room": "Saulėtekio al.7, Vilnius\nV103 kab.",
   "Name": "Kalcienė Virginija",
   "Occupation": "Dr., Asistentė, Jaunesnioji administratorė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8205",
   "Email0": "lilija.kalediene@gf.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nC345 kab.",
   "Name": "Kalėdienė Lilija",
   "Occupation": "Dr. (HP), Profesorė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "ausra.kamarauskaite@gmc.vu.lt",
   "Room": "",
   "Name": "Kamarauskaitė Aušra",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8210",
   "Email0": "ruta.kananaviciute@gf.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nC349 kab.",
   "Name": "Kananavičiūtė Rūta",
   "Occupation": "Dr., Asistentė, Mokslo darbuotoja",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "zana.kapustina@gf.vu.lt",
   "Room": "Geležinio vilko g. 29A",
   "Name": "Kapustina Žana",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "emilija.karazijaite@gmc.vu.lt",
   "Room": "",
   "Name": "Karazijaitė Emilija",
   "Occupation": "Jaunesnioji laborantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8207",
   "Email0": "arnoldas.kaunietis@gmc.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nC335 kab.",
   "Name": "Kaunietis Arnoldas",
   "Occupation": "Dr., Mokslo darbuotojas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8260",
   "Email0": "jurate.mierauskiene@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC231 kab.",
   "Name": "Kazlauskaitė Jūratė",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "(8 5) 278 6804",
   "Email0": "inga.kildusiene@gmail.com",
   "Room": "",
   "Name": "Kildušienė Inga",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "tatjana.kirtikliene@gmc.vu.lt",
   "Room": "",
   "Name": "Kirtiklienė Tatjana",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8232",
   "Email0": "vilma.kisnieriene@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV125 kab.",
   "Name": "Kisnierienė Vilma",
   "Occupation": "Dr., Docentė, Mokslo darbuotoja, Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "juras.kisonas@gmc.vu.lt",
   "Room": "",
   "Name": "Kišonas Juras",
   "Occupation": "Doktorantas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8248",
   "Email0": "violeta.kleizaite@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV423 kab.",
   "Name": "Kleizaitė Violeta",
   "Occupation": "Dr., Vyresnioji mokslo darbuotoja",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8242",
   "Email0": "aleksandras.konovalovas@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC369 kab.",
   "Name": "Konovalovas Aleksandras",
   "Occupation": "Dr., Mokslo darbuotojas, Asistentas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "svetlana.kozupejeva@gmc.vu.lt",
   "Room": "",
   "Name": "Kozupejeva Svetlana",
   "Occupation": "Jaunesnioji laborantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "viktorija.kralikiene@gmc.vu.lt",
   "Room": "",
   "Name": "Kralikienė Viktorija",
   "Occupation": "Laborantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8230",
   "Email0": "renatas.krasauskas@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC355 kab. kab.",
   "Name": "Krasauskas Renatas",
   "Occupation": "Jaunesnysis laborantas, Doktorantas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "raimonda.kubiliute@gf.vu.lt",
   "Room": "",
   "Name": "Kubiliūtė Raimonda",
   "Occupation": "Doktorantė, Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8213",
   "Email0": "nomeda.kuisiene@gf.vu.lt",
   "Room": "Saulėtekio al.7,Vilnius\nC333 kab.",
   "Name": "Kuisienė Nomeda",
   "Occupation": "Dr., Profesorė, Vyriausioji mokslo darbuotoja-projekto mokslinė vadovė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "ugne.kuliesiute@gmc.vu.lt",
   "Room": "",
   "Name": "Kuliešiūtė Ugnė",
   "Occupation": "Laborantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "linas.kunigenas@gmc.vu.lt",
   "Room": "",
   "Name": "Kunigėnas Linas",
   "Occupation": "Doktorantas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8250",
   "Email0": "ernestas.kutorga@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV141 kab.",
   "Name": "Kutorga Ernestas",
   "Occupation": "Dr. (HP), Profesorius, Vyresnysis mokslo darbuotojas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8270",
   "Email0": "viktorija.kuznecova@gf.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nC133 kab.",
   "Name": "Kuznecova Viktorija",
   "Occupation": "Laborantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "9438",
   "Email0": "kotryna.kvederaviciute@mif.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV415 kab.",
   "Name": "Kvederavičiūtė Kotryna",
   "Occupation": "Jaunesnioji asistentė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8228",
   "Email0": "danute.labeikyte@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV243 kab.",
   "Name": "Labeikytė Danutė",
   "Occupation": "Dr., Docentė, Mokslo darbuotoja",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "indre.lapeikaite@gf.vu.lt",
   "Room": "",
   "Name": "Lapeikaitė Indrė",
   "Occupation": "Jaunesnioji mokslo darbuotoja, Jaunesnioji asistentė, Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8210",
   "Email0": "jolanta.lebedeva@gf.vu.lt",
   "Room": "",
   "Name": "Lebedeva Jolanta",
   "Occupation": "Jaunesnioji asistentė, Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8207",
   "Email0": "jolanta.lekaviciute@gf.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nC335 kab.",
   "Name": "Lekavičiūtė Jolanta",
   "Occupation": "Vyresnioji laborantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "daiva.lesciute-krilaviciene@gmc.vu.lt",
   "Room": "Saulėtekio al. 9, III rūmai",
   "Name": "Leščiūtė-Krilavičienė Daiva",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "vaida.markovskiene@gmc.vu.lt",
   "Room": "",
   "Name": "Markovskienė Vaida",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8243",
   "Email0": "arvydas.markuckas@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC353 kab.",
   "Name": "Markuckas Arvydas",
   "Occupation": "Dr., Docentas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "augustinas.matulevicius@mf.vu.lt",
   "Room": "",
   "Name": "Matulevičius Augustinas",
   "Occupation": "Doktorantas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8253",
   "Email0": "audrone.meldziukiene@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV145 kab.",
   "Name": "Meldžiukienė Audronė",
   "Occupation": "Lektorė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "223 4449",
   "Email0": "sigita.melynyte@gf.vu.lt",
   "Room": "Sauletekio al.7,Vilnius\nC143, V115 kab.",
   "Name": "Mėlynytė Sigita",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8242",
   "Email0": "algirdas.mikalkenas@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC369 kab.",
   "Name": "Mikalkėnas Algirdas",
   "Occupation": "Jaunesnysis laborantas, Jaunesnysis asistentas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "37068479958",
   "Email0": "rasa.monciunskaite@gf.vu.lt",
   "Room": "",
   "Name": "Mončiunskaitė Rasa",
   "Occupation": "Jaunesnioji mokslo darbuotoja, Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8260",
   "Email0": "vaidotas.morkunas@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC231 kab.",
   "Name": "Morkūnas Vaidotas",
   "Occupation": "Dr., Vyresnysis mokslo darbuotojas, Docentas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "juste.navickaite@gmc.vu.lt",
   "Room": "",
   "Name": "Navickaitė Justė",
   "Occupation": "Jaunesnioji laborantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8750",
   "Email0": "irena.nedveckyte@gf.vu.lt",
   "Room": "Elektrinės g. 6-21, Vilnius\nV103 kab.",
   "Name": "Nedveckytė Irena",
   "Occupation": "Dr., Asistentė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "223 4437",
   "Email0": "urte.neniskyte@gf.vu.lt",
   "Room": "v113 kab.",
   "Name": "Neniškytė Urtė",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "223 4441",
   "Email0": "ausrine.nestarenkaite@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC205 kab.",
   "Name": "Nestarenkaitė Aušrinė",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "223 4433",
   "Email0": "vytautas.oskinis@gmc.vu.lt",
   "Room": "Saulėtekio al.7, Vilnius\nV105 kab.",
   "Name": "Oškinis Vytautas",
   "Occupation": "Dr., Asistentas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8231",
   "Email0": "ramute.pagalyte@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC363 kab.",
   "Name": "Pagalytė Ramutė",
   "Occupation": "Vyresnioji laborantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "223 4437",
   "Email0": "daina.pamedytyte@gmc.vu.lt",
   "Room": "V113 kab.",
   "Name": "Pamedytytė Daina",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "vykinta.parciauskaite@gf.vu.lt",
   "Room": "",
   "Name": "Parčiauskaitė Vykinta",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "219 3137",
   "Email0": "izolda.pasakinskiene@gf.vu.lt",
   "Room": "Kairėnų g. 45, 2040 Vilnius\n203 kab.",
   "Name": "Pašakinskienė Izolda",
   "Occupation": "Habil. dr., Profesorė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "223 4443",
   "Email0": "jolanta.patamsyte@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV425 kab.",
   "Name": "Patamsytė Jolanta",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "tomas.paulauskas@gf.vu.lt",
   "Room": "",
   "Name": "Paulauskas Tomas",
   "Occupation": "Doktorantas, Specialistas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8207",
   "Email0": "raimonda.petkauskaite@gf.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nC335 kab.",
   "Name": "Petkauskaitė Raimonda",
   "Occupation": "Jaunesnioji asistentė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8267",
   "Email0": "andrius.petrasiunas@gf.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nC121 kab.",
   "Name": "Petrašiūnas Andrius",
   "Occupation": "Dr., Docentas, Mokslo darbuotojas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "evaldas.pipinis@gmc.vu.lt",
   "Room": "",
   "Name": "Pipinis Evaldas",
   "Occupation": "Jaunesnysis asistentas, Doktorantas, Specialistas (tyrėjas)",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8224",
   "Email0": "vida.piskarskiene@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV239 kab.",
   "Name": "Piskarskienė Vida",
   "Occupation": "Laborantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8227",
   "Email0": "alius.pleskaciauskas@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV127 kab.",
   "Name": "Pleskačiauskas Aleksandras",
   "Occupation": "Dr., Lektorius, Jaunesnysis mokslo darbuotojas, Jaunesnysis administratorius",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "223 4430",
   "Email0": "sigitas.podenas@gf.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nC125 kab.",
   "Name": "Podėnas Sigitas",
   "Occupation": "Dr. (HP), Profesorius",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "223 4430",
   "Email0": "virginija.podeniene@gf.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nC125 kab.",
   "Name": "Podėnienė Virginija",
   "Occupation": "Dr., Docentė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8201",
   "Email0": "ingrida.prigodina@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC149 kab.",
   "Name": "Prigodina Lukošienė Ingrida",
   "Occupation": "Dr., Asistentė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "vilmantas.pupkis@gmc.vu.lt",
   "Room": "",
   "Name": "Pupkis Vilmantas",
   "Occupation": "Specialistas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "85 210 2741",
   "Email0": "gedmante.radziuviene@gmc.vu.lt",
   "Room": "",
   "Name": "Radžiuvienė Gedmantė",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "vytautas.rafanavicius@gf.vu.lt",
   "Room": "",
   "Name": "Rafanavičius Vytautas",
   "Occupation": "Doktorantas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8264",
   "Email0": "rimantas.rakauskas@gf.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nC123 kab.",
   "Name": "Rakauskas Rimantas",
   "Occupation": "Habil. dr., Profesorius, Vyriausiasis mokslo darbuotojas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8249",
   "Email0": "vytautas.rancelis@gf.vu.lt",
   "Room": "Saulėtekio al.7, Vilnius\nC502 kab.",
   "Name": "Rančelis Vytautas Petras",
   "Occupation": "Habil. dr., Profesorius emeritas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8263 223 4355",
   "Email0": "mindaugas.rasimavicius@gf.vu.lt",
   "Room": "Saulėtekio al.7, Vilnius\nV147 kab.",
   "Name": "Rasimavičius Mindaugas",
   "Occupation": "Dr., Jaunesnysis mokslo darbuotojas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8208",
   "Email0": "juozas.raugalas@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC337 kab.",
   "Name": "Raugalas Juozas",
   "Occupation": "Dr., Lektorius",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "2239 8253",
   "Email0": "radvile.rimgaile-voicik@gmc.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV145 kab.",
   "Name": "Rimgailė-Voicik Radvilė",
   "Occupation": "Dr., Mokslo darbuotoja, Asistentė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8222",
   "Email0": "osvaldas.ruksenas@gf.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nV131 kab.",
   "Name": "Rukšėnas Osvaldas",
   "Occupation": "Dr. (HP), Profesorius",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8251",
   "Email0": "jone.rukseniene@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV111 kab.",
   "Name": "Rukšėnienė Jonė",
   "Occupation": "Dr., Biologė tyrėja",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "219 3222",
   "Email0": "arunas.samas@gf.vu.lt",
   "Room": "Eitminų g. 12-133, Vilnius\nV101 kab.",
   "Name": "Samas Arūnas",
   "Occupation": "Dr., Asistentas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8225",
   "Email0": "ausra.sadauskaite@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV235 kab.",
   "Name": "Sasnauskienė Aušra",
   "Occupation": "Dr., Mokslo darbuotoja, Docentė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "223 4437",
   "Email0": "lina.saveikyte@gmc.vu.lt",
   "Room": "V113 kab.",
   "Name": "Saveikytė Lina",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "jurgita.sejoniene@gmc.vu.lt",
   "Room": "",
   "Name": "Sejonienė Jurgita",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8244",
   "Email0": "saulius.serva@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC371 kab.",
   "Name": "Serva Saulius",
   "Occupation": "Dr., Profesorius",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8230",
   "Email0": "jurate.skerniskyte@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC355 kab.",
   "Name": "Skerniškytė Jūratė",
   "Occupation": "Jaunesnioji asistentė, Jaunesnioji mokslo darbuotoja",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8266",
   "Email0": "saulis.skuja@gf.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nC117 kab.",
   "Name": "Skuja Saulis",
   "Occupation": "Lektorius",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8270",
   "Email0": "grita.skujiene@gf.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nC133 kab.",
   "Name": "Skujienė Grita",
   "Occupation": "Dr., Asistentė, Muziejininkė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8258",
   "Email0": "grazina.slapsyte@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC229 1 kab.",
   "Name": "Slapšytė Gražina",
   "Occupation": "Dr. (HP), Profesorė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "narvydas.stankevicius@gmc.vu.lt",
   "Room": "",
   "Name": "Stankevičius Narvydas",
   "Occupation": "Restauratorius",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8259",
   "Email0": "asta.stapulionyte@gf.vu.lt",
   "Room": "Saulėtekio al. 7, GMC Biomokslų i-tas, Vilnius\nC235 kab.",
   "Name": "Stapulionytė Asta",
   "Occupation": "Laborantė, Jaunesnioji asistentė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "vytaute.starkuviene@gmc.vu.lt",
   "Room": "",
   "Name": "Starkuvienė Vytautė",
   "Occupation": "Dr., Profesorė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "dovile.strepetkaite@gmc.vu.lt",
   "Room": "",
   "Name": "Strepetkaitė Dovilė",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "223 4442",
   "Email0": "kristina.stuopelyte@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC207 kab.",
   "Name": "Stuopelytė Kristina",
   "Occupation": "Jaunesnioji asistentė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "vaida.surviliene@gf.vu.lt",
   "Room": "Saulėtekio al. 7, LT- 10275, Vilnius",
   "Name": "Survilienė Vaida",
   "Occupation": "Jaunesnioji mokslo darbuotoja, Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8226",
   "Email0": "edita.suziedeliene@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC361 kab.",
   "Name": "Sužiedėlienė Edita",
   "Occupation": "Dr. (HP), Profesorė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "lauradagilyte@yahoo.com",
   "Room": "",
   "Name": "Šiaulienė Laura",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8259",
   "Email0": "liucija.simanskiene@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC235 kab.",
   "Name": "Šimanskienė Liucija",
   "Occupation": "Laborantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "dovile.simkute@gmc.vu.lt",
   "Room": "",
   "Name": "Šimkutė Dovilė",
   "Occupation": "Jaunesnioji asistentė, Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8261",
   "Email0": "raimondas.siuksta@gf.vu.lt",
   "Room": "Saulėtekio al. 7\nv429 kab.",
   "Name": "Šiukšta Raimondas",
   "Occupation": "Dr., Docentas, Mokslo darbuotojas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "akvile.slektaite@gmc.vu.lt",
   "Room": "",
   "Name": "Šlėktaitė Akvilė",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8227",
   "Email0": "alvydas.soliunas@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV127 kab.",
   "Name": "Šoliūnas Alvydas",
   "Occupation": "Dr., Lektorius, Laborantas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8203",
   "Email0": "lina.svambariene@gmc.vu.lt",
   "Room": "Saulėtekio al. 7,Vilniu\nC251 kab.",
   "Name": "Švambarienė Lina",
   "Occupation": "Personalo ir finansų administratorė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "povilas.tarailis@gmc.vu.lt",
   "Room": "",
   "Name": "Tarailis Povilas",
   "Occupation": "Doktorantas, Specialistas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "219 3222",
   "Email0": "giedrius.trakimas@gf.vu.lt",
   "Room": "Saulėtekio al.7, Vilnius\nV101 kab.",
   "Name": "Trakimas Giedrius",
   "Occupation": "Lektorius",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8253",
   "Email0": "jurate.tupciauskaite@gf.vu.lt",
   "Room": "Saulėtekio al.7, Vilnius\nV145 kab.",
   "Name": "Tupčiauskaitė Jūratė Juzė",
   "Occupation": "Dr., Biologė tyrėja",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8265",
   "Email0": "jurga.turcinaviciene@gf.vu.lt",
   "Room": "Saulėtekio al. 7,Vilnius\nC151,C119 kab.",
   "Name": "Turčinavičienė Jurga",
   "Occupation": "Dr., Docentė, Mokslo darbuotoja",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8299",
   "Email0": "alius.ulevicius@gf.vu.lt",
   "Room": "Saulėtekio al.7, Vilnius\nV107 kab.",
   "Name": "Ulevičius Alius",
   "Occupation": "Dr., Profesorius",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "deimante.usaite@gmc.vu.lt",
   "Room": "",
   "Name": "Ūsaitė Deimantė",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8248",
   "Email0": "virginija.vaitkuniene@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV423 kab.",
   "Name": "Vaitkūnienė Virginija",
   "Occupation": "Dr., Mokslo darbuotoja",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "vaidotas.valskys@gmc.vu.lt",
   "Room": "A. Juozapavičiaus g. 6, Vilnius",
   "Name": "Valskys Vaidotas",
   "Occupation": "Dr., Asistentas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8254",
   "Email0": "petras.venckus@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV113 kab.",
   "Name": "Venckus Petras",
   "Occupation": "Jaunesnysis asistentas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "valentina.vengeliene@gf.vu.lt",
   "Room": "",
   "Name": "Vengelienė Valentina",
   "Occupation": "Dr., Profesorė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "aleksandras.voicikas@gf.vu.lt",
   "Room": "",
   "Name": "Voicikas Aleksandras",
   "Occupation": "Dr., Jaunesnysis mokslo darbuotojas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "aurelijus.zimkus@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC351 kab.",
   "Name": "Zimkus Aurelijus",
   "Occupation": "Dr., Vyresnysis mokslo darbuotojas, Docentas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "algirdas.zalimas@gmc.vu.lt",
   "Room": "",
   "Name": "Žalimas Algirdas",
   "Occupation": "Doktorantas",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "karolina.zilionyte@gmc.vu.lt",
   "Room": "",
   "Name": "Žilionytė Karolina",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "diana.zilovic@gmc.vu.lt",
   "Room": "",
   "Name": "Žilovič Diana",
   "Occupation": "Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "238 8241",
   "Email0": "zigmantas.zitkus@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nC351 kab.",
   "Name": "Žitkus Zigmantas",
   "Occupation": "Lektorius",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "",
   "Email0": "vilmante.zitkute@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV237 kab.",
   "Name": "Žitkutė Vilmantė",
   "Occupation": "Jaunesnioji laborantė, Doktorantė",
   "Institute": "Institute of Bioscience"
 },
 {
   "Phone": "239 8252",
   "Email0": "donatas.zvingila@gf.vu.lt",
   "Room": "Saulėtekio al. 7, Vilnius\nV427 kab.",
   "Name": "Žvingila Donatas",
   "Occupation": "Dr. (HP), Profesorius",
   "Institute": "Institute of Bioscience"
 }
]