
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import PersonIcon from '@material-ui/icons/Person';
import FlaskIcon from 'mdi-material-ui/Flask';
import ScopeIcon from 'mdi-material-ui/Microscope';
import DnaIcon from 'mdi-material-ui/Dna';

const useStyles = makeStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
});

let iconsArray = [<PersonIcon />, <ScopeIcon />,<FlaskIcon />,<DnaIcon />];

export default function TemporaryDrawer(props) {
  const classes = useStyles();
  const [state, setState] = React.useState({
    left: false,
    

  });
  const [selectedIndex, setSelectedIndex] = React.useState(0);

  function handleListItemClick(event, index, text) {
    setSelectedIndex(index);
    props.onListItemClick(text);
    
  }

  const toggleDrawer = (side, open) => event => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, [side]: open });
  };
  
  if(props.toggleDrawer){
      toggleDrawer("left", !state.left);
      props.toggleDrawer = false;
      console.log("Drawer toggle");
      
      
 }
 
 React.useEffect(() => {
     console.log("Updated");
 })

  const sideList = side => (
    <div
      className={classes.list}
      role="presentation"
      onClick={props.onMenuClick}
      onKeyDown={props.onMenuClick}
    >
      <List>
        {
          ['People search', 'Equipment Search', 'Experiment Search', 'Compounds Search'].map((text, index) => (
          <ListItem button key={text}
          selected={selectedIndex === index}
          onClick={event => handleListItemClick(event, index, text)}>
            <ListItemIcon>{iconsArray[index]}</ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
    </div>
  );

  return (
    <div>
      <Drawer open={props.isDrawerOpen} onClose={props.onMenuClick}>
        {sideList('left')}
      </Drawer>

    </div>
  );
}
