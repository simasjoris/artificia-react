import React from 'react';
import './App.css';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

import CallIcon from '@material-ui/icons/Call';
import EmailIcon from '@material-ui/icons/Email';
import HomeIcon from '@material-ui/icons/Home';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import logo from './logo_alpha.png';


const useStyles = makeStyles(theme => ({
  contact: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  icon: {
    margin: theme.spacing(1),
  },
  list: {
    backgroundColor: theme.palette.background.paper,

  },
  card: {
    minWidth: 275,
    flex: 1,

  },
  title: {
    fontSize: 20,
  },
  noresults: {
    marginTop: '30px'
  },
  contactsTitle: {
    fontSize: 20,
  },
  pos: {
    marginBottom: 12,
  },
  logo: {
    [theme.breakpoints.down('xs')]: {
      width: '100%'
    },
  }
}));

export default function CardList(props) {
  const classes = useStyles();

  function getPubmedLink(name) {
    return `https://www.ncbi.nlm.nih.gov/pubmed/?term=${name}[Author]`
  }
  
  function renderList(){
    return(
      <List className={classes.list}>
        {props.responseArray.map((author, index) => (
          <ListItem key={index}>
            <Card className={classes.card}>
              <CardContent>
                <Grid container spacing={3}>
                  <Grid item xs={12} sm={6}>
                    <Typography className={classes.title}>
                      {author.Name || "Not given"}
                    </Typography>
                    <Typography className={classes.pos} color="textSecondary">
                      {author.Occupation || "Not given"}
                    </Typography>
                    <Typography className={classes.pos} color="textSecondary">
                    <a target="_blank" rel="noopener noreferrer" href={getPubmedLink(author.Name)}>Publications</a>
                    </Typography>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Typography className={classes.contactsTitle} gutterBottom>
                      Contacts
                              </Typography>
                    <div className={classes.contact}>
                      <HomeIcon className={classes.icon} />
                      <Typography variant="body2" component="p">
                        {author.Room || "Not given"}, {author.Institute || "Institute not given"}
                      </Typography>
                    </div>
                    <div className={classes.contact}>
                      <EmailIcon className={classes.icon} />
                      <Typography variant="body2" component="p">
                        {author.Email0 || author.Email1 || "Not given"}
                      </Typography>
                    </div>
                    <div className={classes.contact}>
                      <CallIcon className={classes.icon} />
                      <Typography variant="body2" component="p">
                        {author.Phone || "Not given"}
                      </Typography>
                    </div>
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
          </ListItem>
        ))}
      </List>
    )
  }
  function renderText(text){
    return(
      <Typography component="h1" className={classes.noresults}>
        {text}
      </Typography>
    );
  }

  function render() {
    if(!props.responseArray) {
      return renderIntro('Start by using search field above');
    }
    if(props.responseArray.length > 0) {
      return renderList();
    } else {
      return renderText('No results found');
    }      
  }
  function renderIntro(text){
    return(
      <div className={classes.noresults}>
      <img className={classes.logo} src={logo} alt="Logo"/>
      <Typography component="h1" className={classes.noresults}>
        {text}
      </Typography>
      </div>
    );
  }

  return (
    <div>
      {render()}
    </div>
  );
} 
